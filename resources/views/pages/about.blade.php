@extends('pages.layout.main')

@section('title')
<title>{{$title}}</title>
@endsection

@section('content')
<section id="banner" style="height:150px"> 
  <div class="dark-layer" style="opacity:0.8"></div>
</section>

<!-- About-Conference-1 -->
<section id="about" class="about-section section-padding">
    <div class="container">
      <div class="row"> 
        <!-- About Content -->
        <div class="col-md-6 col-sm-6">
          <div class="about-sec">
            <div class="about-content">
              <h4>LATE HON. CHUKWUDIFU AKUNNE OPUTA</h4>
              <small>
                  (CFR, KSM, KT.CSS, KT SGG)<br>
                  Rtd. Justice of the Supreme Court of Nigeria
               </small>
              <p>{{substr($oputa->data,0,800)}}....<br>
              <a href="#" id="more"  data-url="{{ route('ajax',['id' => $oputa->id]) }}" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Readmore</i></a>
                  </p>
            </div>
            <h5>Justice Oputa Foundation </h5>
            <p>Justice Oputa Foundation (JOF) was established in 2010 and registered as a non-profit, non-governmental association in Nigeria</p>
            <h6>Mission </h6>
            <p>JOF was established to advance and apply knowledge germane to the legal professions, commerce and industry and the public sector through the development, conduct and dissemination of both fundamental and applied research in the areas of governance, law and development. The research process of the Foundation is designed to ensure that its work is relevant, practical and contributes to development in Nigeria and the African continent. It brings researchers together with eminent persons, policy makers/public sector representatives, legal professionals, commerce and industry experts in ongoing conversations and exploration of issues, ideas and analysis of its core areas of concern.</p>
            <h6>JOF Objectives</h6>
            <p>
              <ul>
                <li>To be a globally recognized authority on issues relating to <strong>Governance, Law and Development</strong>. </li>
                <li>Actively promote high standards of ethics and integrity among key stakeholders (policy makers/public sector 
                    representatives, legal professionals, commerce and industry experts and the media) involved in governance and 
                    policy implementation, legal adjudication, and conduct of business in an ethical manner via diverse institutions.</li>
                <li>Promote human rights, law and order and contribute to improvements in Nigeria’s human development index.</li>
                <li>To advance and apply knowledge germane to the legal professions, commerce and industry and the public sector 
                    through the development, conduct and dissemination of both fundamental and applied research in the areas of 
                    governance, law and development</li>
                <li>Improve and develop JOF’s training and publishing activities. </li>
              </ul>
            </p>
            <h5>Justice Oputa Annual Conference</h5>
            <p>The Conference brings together eminent persons, policy makers/public sector representatives, researchers, legal professionals, commerce and industry experts in ongoing conversations and exploration of issues, ideas and analysis of its core areas of concern, which are governance, law and development. 

                <p><strong>JOAC</strong> will put front and centre, the daunting issue of governance, both at the governmental and corporate levels; the need to improve fiscal discipline especially in the area of public financial management systems; and improve human development index, that will result in prosperity for all.</p>
                </p>
          </div>
        </div>
        <!-- /About Content --> 
        
        <!-- Carousel Slider -->
        <div class="col-md-6 col-sm-6 ">
          <div class="about-carousel border-box">
            <div class="carousel slide" id="aboutCarousel" data-ride="carousel"> 
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item" style="border:12px solid #cccccc "><img src="{{ asset('assets/images/oputa.png')}}" alt="" class="center-block" height="750" width="874" ></div>
                <div class="item active" style="border:12px solid silver "><img src="{{asset('assets/images/oputa2.jpg')}}" alt="" class="center-block" height="750" width="874"></div>
                
              </div>
              
              <!-- Left and right controls --> 
              <a class="left carousel-control" href="#aboutCarousel" role="button" data-slide="prev"> <span class="fa fa-angle-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#aboutCarousel" role="button" data-slide="next"> <span class="fa fa-angle-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
          </div>
        </div>
        <!-- /Carousel Slider --> 
        
      </div>
    </div>

    <div class="modal fade" id="readmore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;padding:10px">
        <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
               <h5 id="dynamicName" class="modal-title" id="myModalLabel"></h5>
               <div class="" id="image"></div>
              </div>
                <div class="modal-body">

                  <div id="modal-loader" style="display: none; text-align: center;">
                      <img src="{{asset('assets/images/ajax-loader.gif')}}">
                  </div>

                  <div id="dynamicBio" style="height:auto"></div>
                   
                </div>
                <div class="modal-footer">
                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   
                </div>
             </div><!-- /.modal-content -->
      </div><!-- /.modal -->


  </section>
  <script>
    <script>
           
      </script>
  </script>



@endsection