@if(count($errors) >0 )
    <span style="font-size:18px;color:red">Please fix the following errors</span>
    <ul class="list-group">
        @foreach($errors->all() as $e)
            <li class="list-group-item text-danger">
                {{$e}}
            </li>
        @endforeach
    </ul>

@endif