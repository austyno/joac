<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
{{--<title>JOAC</title>--}}
@yield('title')
<!--Bootstrap -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
<!--Custome Style -->
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
<!--OWL Carousel slider-->
<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/owl.transitions.css')}}" type="text/css">
<!--Pretty-Photo-->
<link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/toastr.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/aos.css')}}" type="text/css">
<!-- SWITCHER -->

<!--FontAwesome Font Style -->
<link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/favicon-icon/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/favicon-icon/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/favicon-icon/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/images/favicon-icon/apple-touch-icon-57-precomposed.png">
{{--<link rel="shortcut icon" href="assets/images/favicon-icon/favicon.png">--}}
<!-- Google-Font-->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
<script src="{{ asset('assets/js/common.js')}}" charset="UTF-8" type="text/javascript"></script>
<script src="{{ asset('assets/js/util.js')}}" charset="UTF-8" type="text/javascript"></script>
<script src="{{ asset('assets/js/stats.js')}}" charset="UTF-8" type="text/javascript"></script>
</head>
<body id="home">

<!--Header-->
<header class="nav-stacked affix" data-spy="affix" data-offset-top="15">
  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-md-2">
        <div class="logo" style="margin-bottom:2px"> 
          
          <img src="{{ asset('assets/images/logo.jpg') }}" height="70px" style="border-radius:10px;border:3px solid silver;height:95px;"/>
      </div>
      </div>
      <div class="col-sm-9 col-md-10"> 
        <!-- Navigation -->
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
              <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
            </button>
          </div>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
            <li><a href="{{ route('index') }}" class="js-target-scroll">Home</a></li>
            <li class="dropdown">
              <a href="{{ route('about') }}" class="js-target-scroll">Who We Are</a>
              <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
              <ul class="dropdown-menu">
              <li><a href="{{ route('board') }}">The Team</a></li>                
                </ul>
            </li>
            <li><a href="{{ route('speakers') }}" class="js-target-scroll">Speakers</a></li>
              {{--<li><a href="{{ route('schedule')}}" class="js-target-scroll">Schedule</a></li>--}}
              <li><a href="{{route('programs')}}" class="js-target-scroll">Programs</a></li>
              <li><a href="{{route('pricing')}}" class="js-target-scroll">Book a seat</a></li>
              <li><a href="{{route('Spnsorship')}}" class="js-target-scroll">Spnsorship</a></li>
            {{--<li><a href="{{ route('sponsors') }}" class="js-target-scroll">Sponsors</a></li>--}}
              {{--<li><a href="{{ route('gallery') }}" class="js-target-scroll">Gallery</a></li>--}}
              {{--<li><a href="#" class="js-target-scroll">Resources</a></li>--}}
              {{--<li class="dropdown">
          		<a href="#blog" class="js-target-scroll">Blog</a>
                <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                <ul class="dropdown-menu">
                  <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                  <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                  <li><a href="blog-grid.html">Blog Grid Style</a></li>
                  <li><a href="blog-single.html">Blog Single</a></li>
                </ul>
              </li>--}}
              {{--<li><a href="#venue" class="js-target-scroll">Venue</a></li>--}}
            <li><a href="{{ route('blog') }}" class="js-target-scroll">Blog</a></li>
              {{--<li class="dropdown">
          		<a href="#" class="js-target-scroll">Elements</a>
                <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                <ul class="dropdown-menu">
                  <li><a href="about-us.html">About Us</a></li>
                  <li><a href="gallery.html">Gallery</a></li>
                  <li><a href="pricing.html">Pricing</a></li>
                  <li><a href="schedule.html">Schedule</a></li>
                  <li><a href="speakers.html">Speakers</a></li>
                  <li><a href="sponsors.html">Sponsors</a></li>
                  <li><a href="testimonials.html">Testimonials</a></li>
                  <li><a href="venue.html">Venue</a></li>
                </ul>
              </li>--}}
            </ul>
          </div>
        </nav>
        <!-- Navigation end --> 
      </div>
    </div>
  </div>
</header>
<!-- /Header --> 

<!-- Banners -->
<section class="inner-pages element_header text-center">   
  <div class="dark-layer"></div>
</section>
@yield('content')


<footer class="secondary-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-push-4 text-center"> 
      	<div class="footer_logo">
            <h3><a href="#"><span>JO</span>AC</a></h3>
        </div>
      </div>
      <div class="col-md-4 col-md-push-4">
        <ul class="social_links">
          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-md-pull-8">
        <p>&copy; 2018 All Rights Reserved.</p><br>
        <small>Powered By <a href="#">AppNov Tech</a></small>
      </div>
    </div>
  </div>
</footer>
<!--/footer--> 
<!--Back to top-->
<div style="display: block;" id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
<!--/Back to top--> 

<!-- Scripts -->   
<script src="{{asset('assets/js/jquery.min.js')}}"></script> 
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script> 
<script src="{{asset('assets/js/js.js')}}"></script> 
<script src="{{asset('assets/js/gmaps.min.js')}}"></script> 
<script src="{{asset('assets/js/jquery.validate.min.js')}}"></script> 
<script src="{{ asset('assets/js/interface.js')}}"></script> 
<!-- testimonial-slider-JS--> 
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script> 
<!-- Countdown--> 
<script src="{{asset('assets/js/jquery_003.js')}}"></script>
<!-- prettyPhoto--> 
<script src="gallery_files/jquery_004.js" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('assets/js/jquery.prettyPhoto.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('assets/js/aos.js')}}" type="text/javascript" charset="utf-8"></script>
<script>
  AOS.init({
  duration: 1200,
  easing: 'ease-in-out-sine'
  })
</script>
<script src="{{ asset('assets/js/toastr.min.js')}}"></script>
<script>

  toastr.options = {
  "positionClass": "toast-bottom-right"
}

  @if(Session::has('success'))
    toastr.success("{{ Session::get('success') }}")
  @endif



$('.rep').click(function(e){

    e.preventDefault();

    @if(Auth::guest())
       window.location = "{{url('login')}}";
    @else
    
      var commentId = $(this).data('comment_id');

      $('#commentId').val(commentId);

      $('#myModal').modal().show()

    @endif
});


      $(document).ready(function(){
		    $(document).on('click','#more', function(e){
	
			e.preventDefault();
	
			var url = $(this).data('url');
			$('#modal-loader').show();
	
			$.ajax({
				url : url,
				type : 'GET',
				dataType : 'html'
			})
			.done(function(data){
				data = JSON.parse(data);
				console.log(data);
				$('#image').html("<img class='img-responsive' src='{{ asset('assets/images/oputa.png') }}' style='height:300px;width:300px;margin:auto'>");
				$('#dynamicName').html(data.name);
				$('#dynamicBio').html(data.data);
				$('#modal-loader').hide();
			})
			.fail(function(){
				$('#dynamic').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
				$('#modal-loader').hide();
			});
	
		});
	});

  

</script>

</body></html>