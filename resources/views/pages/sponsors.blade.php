@extends('pages.layout.main')

@section('content')

<section id="banner" style="height:150px"> 
  <div class="dark-layer" style="opacity:0.8"></div>
</section>

<section id="sponsors" class="sponsors_style3 section-padding">
    <div class="container">
      <div class="row"> 
        <!-- Heading -->
        <div class="col-md-12">
          <div class="heading-sec">
            <div class="section-header text-center">
              <h2>Sponsors</h2>
              <p>About Our Sponsors</p>
            </div>
          </div>
        </div>
        <!-- /Heading -->
  
        <div class="col-md-12">
            <div id="sponsors_table" class="table-responsive">
              <table>
                  <tbody><tr>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_1.png" alt="image"></a></td>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_3.png" alt="image"></a></td>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                  </tr>
                  <tr>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_3.png" alt="image"></a></td>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                      <td><a href="#"><img src="sponsors2_files/sponsors_logo_1.png" alt="image"></a></td>
                  </tr>
              </tbody></table>
          </div>
        </div>
        
      </div>
    </div>
  </section>
<!--/Conference Sponsors--> 

<!--Become Sponsors--> 
<section id="become_sponsor" class="section-padding text-center">
<div class="container">
  <div class="white_text div_zindex">
      <h2>Become a Sponsor</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
    <div class="space-30"></div>
    <a href="#" class="btn margin-btm-20">Download PDF</a>
    <a href="#" class="btn margin-btm-20">Get Started Today</a>
</div>
</div>
<div class="dark-layer"></div>
</section>
@endsection