@extends('pages.layout.main')

@section('content')

<section id="banner" style="height:150px"> 
  <div class="dark-layer" style="opacity:0.8"></div>
</section>

<section class="section-padding">
   <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="heading-sec clearfix">
              <div class="section-header text-center">
                <h2>Past Events</h2>
                <p>Event Gallery </p>
              </div>
            </div>
          </div>
        </div>
        
        <!-- Event Gallery -->
        <div class="gallery_style3 row">
        	<div class="col-md-4">
            	<a href="http://themes.webmasterdriver.net/BeEvent/demo-2/with-eventbrite/assets/images/gallery_img1.jpg" rel="prettyPhoto[pp_gal]">
                    <img src="{{ asset('assets/images/about-event-img4.jpg') }}" alt="">
                    <div class="gallery_title ">Presentations 2016 </div>
                </a>
            </div>
            
		    <div class="col-md-4">
                <a href="http://themes.webmasterdriver.net/BeEvent/demo-2/with-eventbrite/assets/images/gallery_img2.jpg" rel="prettyPhoto[pp_gal]">
                <img src="{{ asset('assets/images/about-event-img4.jpg') }}" alt="">
                    <div class="gallery_title ">Presentations 2016</div>
                </a>
            </div>
            
            <div class="col-md-4">
                <a href="http://themes.webmasterdriver.net/BeEvent/demo-2/with-eventbrite/assets/images/gallery_img3.jpg" rel="prettyPhoto[pp_gal]">
                <img src="{{ asset('assets/images/about-event-img4.jpg') }}" alt="">
                    <div class="gallery_title ">Presentations 2016</div>
                </a>
            </div>
            
            <div class="col-md-4">
                <a href="http://themes.webmasterdriver.net/BeEvent/demo-2/with-eventbrite/assets/images/gallery_img4.jpg" rel="prettyPhoto[pp_gal]">
                <img src="{{ asset('assets/images/about-event-img4.jpg') }}" alt="">
                    <div class="gallery_title ">Presentations 2016</div>
                </a>
            </div>
            
		    <div class="col-md-4">
                <a href="http://themes.webmasterdriver.net/BeEvent/demo-2/with-eventbrite/assets/images/gallery_img5.jpg" rel="prettyPhoto[pp_gal]">
                <img src="{{ asset('assets/images/about-event-img4.jpg') }}" alt="">
                    <div class="gallery_title ">Presentations 2016</div>
                </a>
            </div>
            
            <div class="col-md-4">
                <a href="http://themes.webmasterdriver.net/BeEvent/demo-2/with-eventbrite/assets/images/gallery_img7.jpg" rel="prettyPhoto[pp_gal]">
                <img src="{{ asset('assets/images/about-event-img4.jpg') }}" alt="">
                    <div class="gallery_title ">Presentations 2016</div>
                </a>
            </div>        
        </div>
        <!-- /Event Gallery --> 
   </div>    
</section>

@endsection