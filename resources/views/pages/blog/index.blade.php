
@extends('pages.layout.main')

@section('content')
<section id="banner" style="height:150px"> 
        <div class="dark-layer" style="opacity:0.8"></div>
</section>

<!-- Our Articles -->
<section class="our_articles">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <!-- article-1 -->

                    @foreach($post as $p)
                    <article class="blog_wrap view_one">
                        <div class="blog_img margin-btm-20">
                            <a href="{{ route('show',['slug'=> $p->slug]) }}"><img src="{{asset($p->image)}}" alt="image" height="510px" width="800px"></a>
                        </div>
                        <div class="blog_meta">
                        <p> {{$p->created_at->diffForHumans()}} </p>
                        <p><a href="{{ route('catposts',['catname' => $p->category->name]) }}">{{$p->category->name}}</a></p>
                        <p><i class="fa fa-comment"> {{$p->comments->count()}}</i></p>
                        </div>
                        <h3><a href="{{ route('show',['slug'=> $p->slug]) }}">{{$p->title}}</a></h3>
                        <p>{{substr($p->content,0,350)}}.</p>
                    </article>
                  @endforeach
                     {{$post->links()}}
                </div>
                
                <!-- Sidebar -->
                <aside class="col-md-4">
                    <div class="sidebar_wrap">
                    <div class="sidebar_widgets">
                        <div class="widget_title white_text secondary-bg">
                            <h6><i class="fa fa-search" aria-hidden="true"></i> Search Posts</h6>
                        </div>
                        <div class="search_post">
                            <form action="" method="get">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Search..." type="text">
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    <div class="sidebar_widgets">
                        <div class="widget_title white_text secondary-bg">
                            <h6><i class="fa fa-file-text-o" aria-hidden="true"></i> Popular Posts</h6>
                        </div>
                        <div class="popular_post">
    
                           
                            <ul class="list-style-none">
                                    @foreach($popular as $p)
                                <li>
                                    <div class="widget_post_img">
                                        <a href="{{ route('show',['slug' => $p->slug]) }}"><img src="{{asset($p->image)}}" alt="image"></a>
                                    </div>	
                                    <div class="widget_post_info">
                                    <h6><a href="{{ route('show',['slug' => $p->slug]) }}">{{$p->title}}</a></h6>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                            
                        </div>
                    </div>
                    
                    <div class="sidebar_widgets">
                        <div class="widget_title white_text secondary-bg">
                            <h6><i class="fa fa-file-text-o" aria-hidden="true"></i> Categories</h6>
                        </div>
                        <div class="blog_categories">
                            @foreach($cat as $c)
                            <ul class="list-style-none">
                            <li><a href="{{ route('catposts',['catname' => $c->name]) }}"><i class="fa fa-angle-right" aria-hidden="true"></i>{{ $c->name }}</a></li>
                            </ul>
                            @endforeach
                        </div>
                    </div>
                  </div>
                </aside>
                
            </div>
        </div>
    </section>

@endsection