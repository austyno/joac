@extends('pages.layout.main')

@section('content')
<section class="our_articles single_article">
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
            	<!-- article-1 -->
            	<article class="blog_wrap view_one">
                	
                <h3><i>{{ $post->title }}</i></h3>
                    <div class="blog_img margin-btm-20">
                       <img src="{{asset($post->image)}}" alt="image" width="800px" height="450px">
                    </div>
                    <div class="blog_meta">
                        <p style="font-size:19px;color:#228dcb;"><strong>{{ $post->created_at->diffForHumans() }}  {{$post->created_at}}</strong></p>
                        <p><i style="font-size:19px;color:#228dcb;" class="fa fa-comment"> {{$post->comments->count()}}</i></p>
                            <div class="inline-div btn-group-xs categories_in"> 
                            <a href="{{ route('catposts',['catname' => $post->category->name]) }}" class="btn btn-xs">{{$post->category->name}}</a>
                                {{--<a href="#" class="btn btn-xs">Trends</a>
                                <a href="#" class="btn btn-xs">Events</a>--}}
                            </div>
                              
                      </div>
                    <br>
                <p>{{ $post->content }}.</p>
                    
                    <div class="article_tag gray-bg">
                    	<div class="row">
                    		<div class="col-md-6 col-sm-6">
                                <h6>Tags:</h6>
                                <div class="tag_list btn-group-xs">
                                   <a href="{{ route('catposts',['catname' => $post->category->name]) }}" class="btn btn-xs">{{$post->category->name}}</a>&nbsp;&nbsp;&nbsp;
                                   
                                   {{--<a href="#" class="btn btn-xs">Trends</a>
                                   <a href="#" class="btn btn-xs">Events</a>--}}
                                </div>
                            </div>
                            
                            <div class="col-md-6 col-sm-6">
                                <div class="share_article ">
                                	<h6>Share:</h6>
                	                <ul>
        		                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    		                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                         <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
	                                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
              </article>
              
              <!--Comments-->
                <div class="articale_comments">
                	<div id="comments">
                    <h5 class="block-head"></h5>
                        <ul class="commentlist">

                            @foreach($post->comments as $com)
                        	<li class="comment">
                            	<div class="comment-body">
                                	<div class="comment-author">
                                    	<img class="avatar" src="{{asset($com->user->avatar)}}" alt="image">
                                    <span class="fn">{{ $com->user->name }}</span>
                                    </div>
                                <div class="comment-meta commentmetadata"> <a href="#">{{ $com->created_at->diffForHumans() }}</a> </div>
                                    <p>{{$com->comment}}</p>
                                    <div class="reply">
                                    <a href="#" class="rep" class="btn-link" data-comment_id="{{ $com->id }}" ><i class="fa fa-reply" aria-hidden="true"></i> Reply</a>
                                    </div>
                                </div>



                                <ul class="children">

                                    @foreach($com->replies as $r)
                                        <li class="comment">
                                           <div class="comment-body">
                                       <div class="comment-author">
                                       <img class="avatar" src="{{ asset($r->user->avatar) }}" alt="image">
                                       <span class="fn">{{ $r->user->name }}</span>
                                       </div>
                                    <div class="comment-meta commentmetadata"> <a href="#">{{ $r->created_at->diffForHumans() }}</a> </div>
                                    <p>{{ $r->reply }}</p>
                                       <div class="reply">
                                           <a href="#" class="rep" class="btn-link" data-comment_id="{{ $r->id }}"  class="btn-link"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a>
                                       </div>
                                   </div>
                                       </li>
                                    @endforeach
                                   </ul>
                            </li>
                           @endforeach
                           
                            {{--<li class="comment">
                            	<div class="comment-body">
                                	<div class="comment-author">
                                    	<img class="avatar" src="blog-single_files/comment-author-2.jpg" alt="image">
                                    	<span class="fn">John Smith</span>
                                    </div>
                                    <div class="comment-meta commentmetadata"> <a href="#">November 23, 2016 at 12:52 pm</a> </div>
                                    <p>Neque porro quisquam est, qui 
dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia
 non numquam eius modi tempora incidunt ut labore et dolore magnam 
aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum 
exercitationem</p>
                                    <div class="reply">
                                    	<a href="#" class="btn-link"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a>
                                    </div>
                                </div>
                                
                                <ul class="children">
                                	 <li class="comment">
		                            	<div class="comment-body">
                                	<div class="comment-author">
                                    	<img class="avatar" src="blog-single_files/comment-author-3.jpg" alt="image">
                                    	<span class="fn">John Smith</span>
                                    </div>
                                    <div class="comment-meta commentmetadata"> <a href="#">November 23, 2016 at 12:52 pm</a> </div>
                                    <p>Neque porro quisquam est, qui 
dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia
 non numquam eius modi tempora incidunt ut labore et dolore magnam 
aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum 
exercitationem</p>
                                    <div class="reply">
                                    	<a href="#" class="btn-link"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a>
                                    </div>
                                </div>
                                	</li>
                                </ul>
                            </li>--}}
                        </ul>
                    </div>
                </div>
                
                
                <!--Comments-Form-->


                <div class="comment-respond">
                    <h5 class="block-head">Leave a Comment</h5>
                    
                    @include('pages.inc.errors')

                <form  method="post" class="comment-form" action="{{ route('comments',['slug' => $post->slug]) }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                        	<textarea class="form-control" name="comment" cols="" rows="4" placeholder="Comment"></textarea>
                        </div>
                        <input type="hidden" name="post_id" value="{{$post->id}}" >
                        
                        <div class="form-group">
							<button class="btn" type="submit" name="sbtn">Add Comment</button>
                        </div>
                    </form>
                </div>
            </div>
            
            
            <!-- Sidebar -->
            <aside class="col-md-4">
           	 <div class="sidebar_wrap">
            	<div class="sidebar_widgets">
					<div class="widget_title white_text secondary-bg">
	                	<h6><i class="fa fa-search" aria-hidden="true"></i> Search Posts</h6>
                    </div>
                    <div class="search_post">
                    	<form action="" method="get">
                        	<div class="form-group">
                            	<input class="form-control" placeholder="Search..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
                
            	<div class="sidebar_widgets">
					<div class="widget_title white_text secondary-bg">
	                	<h6><i class="fa fa-file-text-o" aria-hidden="true"></i> Popular Posts</h6>
                    </div>
                    <div class="popular_post">

                       
                    	<ul class="list-style-none">
                                @foreach($popular as $p)
                        	<li>
                            	<div class="widget_post_img">
                                <a href="{{ route('show',['slug' => $p->slug]) }}"><img src="{{asset($p->image)}}" alt="image"></a>
                                </div>	
                                <div class="widget_post_info">
                                <h6><a href="{{ route('show',['slug' => $p->slug]) }}">{{$p->title}}</a></h6>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        
                    </div>
                </div>
                
                <div class="sidebar_widgets">
					<div class="widget_title white_text secondary-bg">
	                	<h6><i class="fa fa-file-text-o" aria-hidden="true"></i> Categories</h6>
                    </div>
                    <div class="blog_categories">
                        @foreach($cat as $c)
                    	<ul class="list-style-none">
                            <li><a href="{{ route('catposts',['catname' => $c->name]) }}"><i class="fa fa-angle-right" aria-hidden="true"></i>{{ $c->name }}</a></li>         
                        </ul>
                        @endforeach
                    </div>
                </div>
              </div>
            </aside>
        </div>
    </div>

{{--reply modal--}}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:150px">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                    <h4 class="modal-title" id="myModalLabel">Reply</h4> 
                </div>
                <div class="modal-body">
                        <form   method="post" class="comment-form" action="{{ route('reply') }}">
                            {{ csrf_field() }}
                            
                            <div class="form-group">
                                <textarea class="form-control" name="reply" cols="" rows="4" cols="5" placeholder="Reply"></textarea>
                            </div>
                            <input id="commentId" type="hidden" name="commentId" value="" >
                            
                            <div class="form-group">
                                <button class="btn" type="submit" style="boarder-radius:5px">Add Reply</button>
                            </div>
                        </form>
                </div>
                
            </div><!-- /.modal-content -->
</div>

</section>
@endsection
