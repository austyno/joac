@extends('pages.layout.main')

@section('content')
<section id="schedule" class="schedule_style2 section-padding parallex-bg" style="background-image:url(../assets/images/bg3.jpg);">
  <div class="container div_zindex">
    <div class="row"> 
      <!-- Heading -->
      <div class="col-md-12">
        <div class="heading-sec white_text">
          <div class="section-header text-center">
            <h2>Schedule</h2>
            <p>Event Schedule</p>
          </div>
        </div>
      </div>
      <!-- /Heading -->
      
      <div class="col-md-12">
          {{--<a href="schedule-pdf/pdf-sample.pdf" target="_blank" class="sechedule_download"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download Schedule</a>--}}
           <div class="schedule_wrap">
            <!-- Nav tabs -->
            <ul class="nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#schedule_day1" role="tab" data-toggle="tab">Sunday 2nd December 2018</a></li>
              <li role="presentation"><a href="#schedule_day2" role="tab" data-toggle="tab">Day 1 (3 December 2018)</a></li>
              <li role="presentation"><a href="#schedule_day3" role="tab" data-toggle="tab">Day 2 (4 December 2018)</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content"> 
              <!-- Day 1 Schedule -->
              <div role="tabpanel" class="tab-pane fade in active" id="schedule_day1">
              	<ul class="nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#day1_event1" role="tab" data-toggle="tab">Swimming Pool Area </a></li>
                  <li role="presentation"><a href="#day1_event2" role="tab" data-toggle="tab">Congress Hall</a></li>
                  <li role="presentation"><a href="#day1_event3" role="tab" data-toggle="tab">Foyer</a></li>
                </ul>
                <div class="tab-content"> 
        		      <!-- Day 1 Event 1 -->
		              <div role="tabpanel" class="tab-pane fade in active" id="day1_event1">
                        <div class="schedule_wrap">
                          <p class="schedule_time"><span></span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/george.png')}}" alt="image">
                            <p>Bar George Oputa</p>
                            <small><strong>Chief Executive Officer JOF</strong></small>
                          </div>
                          <div class="schedule_info">
                            <h5>Welcome Reception</h5>
                            <p>On behalf of Justice Oputa Foundation, we are pleased to extend greetings to all prospective participants of the 2018 JOAC Conference to be held in the beautiful City of Abuja, from December 3-4 at the Transcorp Hilton Hotel, Abuja.</p>
                            <p>This year’s theme: First Nigeria; sends a strong message about the need to put Nigeria at the front and centre of all efforts at building a great nation, with strong institutions that guarantee the rights and freedoms of all individuals. A nation that ensures that the fundamental principles of justice and equity are entrenched to forge a united front for tackling the tough challenge of nation building.</p>
                            <p>Finally, we want to ignite our nation to create change and must work together to accomplish the great tasks before us all. In the coming years, we look forward to continuing to exceed the expectations of our stakeholders through programs, workshops, events and activities designed to foster Good Governance, Law and Development, through our partnership with government, business, development partners, corporate leaders and the media. As an advocacy organization, we will also continue to commit ourselves to evolving and expanding our goals to excel within the dynamic legal communities and beyond.</p>
                            <p>JOF warmly invites policy makers, industry leaders, development and funding agencies, professionals and experts from all fields, and all those with an interest in the solidification of good governance, law and development for the economic advancement of Nigeria, to participate in this unique event taking place in the City of Abuja,3-4, December, 2018. </p>
                          </div>
                        </div>
                        {{--<div class="schedule_wrap">
                          <p class="schedule_time">10:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="assets/images/schedule_speaker_2.jpg" alt="image">
                            <p>John Berry</p>
                          </div>
                          <div class="schedule_info">
                            <h4>Registration Opens for Delegates</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                              Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                          </div>
                        </div>--}}
                        
                      </div>
                      <!-- Day 1 Event 2 -->
		              <div role="tabpanel" class="tab-pane fade" id="day1_event2">
                        <div class="schedule_wrap">
                          <p class="schedule_time">08:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/network.png')}}" alt="image">
                            <p></p>
                          </div>
                          <div class="schedule_info">
                            <h5>Exhibitions Bump-In</h5>
                            <p>Participants are expected to visit exibition stands and generally network.</p>
                          </div>
                        </div>
                        {{--<div class="schedule_wrap">
                          <p class="schedule_time">10:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="assets/images/schedule_speaker_2.jpg" alt="image">
                            <p>John Berry</p>
                          </div>
                          <div class="schedule_info">
                            <h4>Speach Heading</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                              Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                          </div>
                        </div>
                        <div class="schedule_wrap">
                          <p class="schedule_time">01:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="assets/images/schedule_speaker_2.jpg" alt="image">
                            <p>John Berry</p>
                          </div>
                          <div class="schedule_info">
                            <h4>Speach Heading</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                              Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                          </div>
                        </div>--}}
                      </div>
                      
                       <!-- Day 1 Event 3 -->
		              <div role="tabpanel" class="tab-pane fade" id="day1_event3">
                      	<div class="schedule_wrap">
                          <p class="schedule_time">10:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/transcorp2.jpg')}}" alt="image">
                            <p></p>
                          </div>
                          <div class="schedule_info">
                            <h5>Registration Opens for Delegates</h5>
                            <p>All participants will be expected to register/confirm registration at the registration desk. All issues related to registrations and other support needs will be handled here</p>
                          </div>
                        </div>
                        {{--<div class="schedule_wrap">
                          <p class="schedule_time">08:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="assets/images/schedule_speaker_2.jpg" alt="image">
                            <p>Samantha Doe</p>
                          </div>
                          <div class="schedule_info">
                            <h4>Welcome</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                              Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                          </div>
                        </div>
                        <div class="schedule_wrap">
                          <p class="schedule_time">10:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="assets/images/schedule_speaker_2.jpg" alt="image">
                            <p>John Berry</p>
                          </div>
                          <div class="schedule_info">
                            <h4>Speach Heading</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                              Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                          </div>
                        </div>--}}

                      </div>
                 </div>       
              </div>
              
              <!-- Day 2 Schedule -->
              <div role="tabpanel" class="tab-pane fade" id="schedule_day2">
              	<ul class="nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#day2_event1" role="tab" data-toggle="tab">Foyer/Congress</a></li>
                  <li role="presentation"><a href="#day2_event2" role="tab" data-toggle="tab">Congress Hall</a></li>
                  <li role="presentation"><a href="#day2_event3" role="tab" data-toggle="tab">Congress Hall</a></li>
                </ul>
                <div class="tab-content"> 
        		      <!-- Day-2 Event-1 -->
		              <div role="tabpanel" class="tab-pane fade in active" id="day2_event1">
                    <div class="schedule_info">
                      <h5>Registration /Breakfast</h5>
                      <p></p>
                    </div>
                        
                  </div>
                      <!-- Day-2 Event-2 -->
		              <div role="tabpanel" class="tab-pane fade" id="day2_event2">
                    <div class="schedule_wrap">
                      <p class="schedule_time">08:00 <span>am</span></p>
                      <div class="schedule_speaker_info"><img src="{{asset('assets/images/karibi.png')}}" alt="image" height="100px">
                        <p>Rtd Justice Adolphus Karibi Whyte</p>
                      </div>
                      <div class="schedule_info">
                        <h5>The Nexus Between Governance, Law and Development</h5>
                        <p></p>
                      </div>
                    </div>
                    <div class="schedule_wrap">
                      <p class="schedule_time">10:00 <span>am</span></p>
                      <div class="schedule_speaker_info"> <img src="{{asset('assets/images/obj.png')}}" alt="image" height="100px">
                        <p>Chief Olusegun Obasanjo</p>
                      </div>
                      <div class="schedule_info">
                        <h5>Conducting Free & Fair, Non-Violent Elections in Nigeria</h5>
                        <p></p>
                      </div>
                    </div>
                  </div>
                      
                      <!-- Day-2 Event-3 -->
		              <div role="tabpanel" class="tab-pane fade" id="day2_event3">
                        <div class="schedule_wrap">
                          <p class="schedule_time">01:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/akanbi.png')}}" alt="image">
                            <p>Prof. Mustapha M. Akanbi SAN</p>
                          </div>
                          <div class="schedule_info">
                            <h5>The Need for Continuous Legal Education for Improved Practice</h5>
                            <p></p>
                          </div>
                        </div>
                        <div class="schedule_wrap">
                          <p class="schedule_time">2:00 <span>pm</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/tam.png')}}" alt="image">
                            <p>Dr. Tam Austin George</p>
                          </div>
                          <div class="schedule_info">
                            <h5>Democracy, Political Parties and Service Delivery</h5>
                            <p></p>
                          </div>
                        </div>
                        <div class="schedule_wrap">
                          <p class="schedule_time">4:00 <span>pm</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/bish.png')}}" alt="image">
                            <p>Bishop Hassan Matthew Kukah PhD</p>
                          </div>
                          <div class="schedule_info">
                            <h5>Rising to the Challenge of Africa’s Development</h5>
                            <p></p>
                          </div>
                        </div>
                      </div>
                 </div>       
              </div>
              
              <!-- Day 3 Schedule -->
              <div role="tabpanel" class="tab-pane fade" id="schedule_day3">
              	<ul class="nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#day3_event1" role="tab" data-toggle="tab">Foyer/Congress</a></li>
                  <li role="presentation"><a href="#day3_event2" role="tab" data-toggle="tab">Congress Hall</a></li>
                  <li role="presentation"><a href="#day3_event3" role="tab" data-toggle="tab">Congress Hall</a></li>
                </ul>
                <div class="tab-content"> 
        		      <!-- Day-3 Event-1 -->
		              <div role="tabpanel" class="tab-pane fade in active" id="day3_event1">
                    <div class="schedule_info">
                      <h5>Registration /Breakfast</h5>
                      <p></p>
                    </div>
                        
                      </div>
                      <!-- Day-3 Event-2 -->
		              <div role="tabpanel" class="tab-pane fade" id="day3_event2">
                        <div class="schedule_wrap">
                          <p class="schedule_time">08:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/prof.png')}}" alt="image">
                            <p>Prof. Charles Q. Dokubo</p>
                          </div>
                          <div class="schedule_info">
                            <h5>Terrorism, Insecurity and the Challenges of Development</h5>
                            <p></p>
                          </div>
                        </div>
                        <div class="schedule_wrap">
                          <p class="schedule_time">10:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/george.png')}}" alt="image">
                            <p>George Oputa Esq</p>
                          </div>
                          <div class="schedule_info">
                            <h5>Legal and Policy Challenges of Improving Nigeria’s Business Operating Environment</h5>
                            <p></p>
                          </div>
                        </div>
                        
                      </div>
                      
                      <!-- Day-3 Event-3 -->
		              <div role="tabpanel" class="tab-pane fade" id="day3_event3">
                    <div class="schedule_wrap">
                      <p class="schedule_time">01:00 <span>am</span></p>
                      <div class="schedule_speaker_info"> <img src="{{asset('assets/images/obi.png')}}" alt="image">
                        <p>HRM Nnaemeka Alfred Nnanyelugo Achebe</p>
                      </div>
                      <div class="schedule_info">
                        <h5>Business Ethics and Corporate Governance</h5>
                        <p></p>
                      </div>
                    </div>
                        <div class="schedule_wrap">
                          <p class="schedule_time">08:00 <span>am</span></p>
                          <div class="schedule_speaker_info"> <img src="{{asset('assets/images/akanbi.png')}}" alt="image">
                            <p>Prof. Mustapha M. Akanbi</p>
                          </div>
                          <div class="schedule_info">
                            <h5>Wealth Transfer Laws in Nigeria</h5>
                            <p></p>
                          </div>
                        </div>
                        <div class="schedule_wrap">
                          <div class="schedule_info">
                            <h4>Patrons’ Gala Dinner</h4>
                            <p></p>
                          </div>
                        </div>
                      </div>
                 </div>       
              </div>
            </div>
  		  </div>
        </div>
      </div>
    </div>
  </div>
  
</section>
@endsection