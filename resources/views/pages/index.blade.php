<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<title>JOAC</title>
<!--Bootstrap -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
<!--Custome Style -->
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
<!--OWL Carousel slider-->
<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/owl.transitions.css')}}" type="text/css">
<!--Pretty-Photo-->
<link rel="stylesheet" href="{{asset('assets/css/prettyPhoto.cs')}}s" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/toastr.min.css')}}" type="text/css">
<link rel="stylesheet" href="{{asset('assets/css/aos.css')}}" type="text/css">
<!-- SWITCHER -->

<!--FontAwesome Font Style -->
<link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/favicon-icon/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/favicon-icon/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/favicon-icon/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/images/favicon-icon/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="{{asset('assets/images/favicon-icon/favicon.png')}}">
<!-- Google-Font-->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body id="home">
        
<!--Header-->
<header class="nav-stacked" data-spy="affix" data-offset-top="15">
  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-md-2">
        <div class="logo"> 
            {{--<h3><a href="#"><span>Be</span>Event</a></h3>--}}
            <img src="{{ asset('assets/images/logo.jpg') }}" height="70px" style="opacity:.9;border-radius:18px;border:5px solid silver"/>
        </div>
      </div>
      <div class="col-sm-9 col-md-10"> 
        <!-- Navigation -->
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button id="menu_slide" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
              <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
            </button>
          </div>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
            <li class="active"><a href="{{ route('index') }}" class="js-target-scroll">Home</a></li>
            <li class="dropdown">
                <a href="{{ route('about') }}" class="js-target-scroll">Who We Are</a>
                <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                <ul class="dropdown-menu">
                <li><a href="{{ route('patrons') }}">Our Patrons</a></li>
                <li><a href="{{ route('board') }}">Board Members</a></li>  
                <li><a href="{{ route('committee') }}">Event Planning Team</a></li>                
                  </ul>
              </li>
              <li><a href="{{ route('speakers') }}" class="js-target-scroll">Speakers</a></li>
              {{--<li><a href="{{ route('schedule')}}" class="js-target-scroll">Schedule</a></li>--}}
              <li><a href="{{route('programs')}}" class="js-target-scroll">Programs</a></li>
              <li><a href="{{route('pricing')}}" class="js-target-scroll">Book a seat</a></li>
              <li><a href="{{route('Spnsorship')}}" class="js-target-scroll">Spnsorship</a></li>
            {{--<li><a href="{{ route('sponsors') }}" class="js-target-scroll">Sponsors</a></li>
              <li><a href="{{ route('gallery') }}" class="js-target-scroll">Gallery</a></li>
              <li><a href="#pricing" class="js-target-scroll">Resources</a></li>
              {{--<li class="dropdown">
          		<a href="#blog" class="js-target-scroll">Blog</a>
                <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                <ul class="dropdown-menu">
                  <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                  <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                  <li><a href="blog-grid.html">Blog Grid Style</a></li>
                  <li><a href="blog-single.html">Blog Single</a></li>
                </ul>
              </li>--}}
              <li><a href="#venue" class="js-target-scroll">Venue</a></li>
            <li><a href="{{ route('blog') }}" class="js-target-scroll">Blog</a></li>
              {{--<li class="dropdown">
          		<a href="#" class="js-target-scroll">Elements</a>
                <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                <ul class="dropdown-menu">
                  <li><a href="about-us.html">About Us</a></li>
                  <li><a href="gallery.html">Gallery</a></li>
                  <li><a href="pricing.html">Pricing</a></li>
                  <li><a href="schedule.html">Schedule</a></li>
                  <li><a href="speakers.html">Speakers</a></li>
                  <li><a href="sponsors.html">Sponsors</a></li>
                  <li><a href="testimonials.html">Testimonials</a></li>
                  <li><a href="venue.html">Venue</a></li>
                </ul>
              </li>--}}
            </ul>
          </div>
        </nav>
        <!-- Navigation end --> 
      </div>
    </div>
  </div>
</header>
<!-- /Header --> 

<!-- Banners -->
<section id="banner" class="banner-section"> 
  <!-- Banner Tagline -->
  <div class="banner-fixed">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 col-md-7 col-lg-8">
          <div class="banner-content">
            <div class="banner-tagline" data-aos="fade-right">
              <h1>Justice Oputa Annual Conference 2018<br><small style="font-size:20px;color:white;font-style:inherit">Theme: GOVERNANCE. LAW. DEVELOPMENT</small></h1>
            </div>
          </div>
        </div>
        <div class="col-sm-5 col-md-5 col-lg-4">
        
        </div>
        </div>
      </div>
    </div>
  </div>

  <section id="timer_countdown" class="vc_row" >
  <div class="col-sm-6 col-md-6  primary-bg vc_col time-back">
    <div class="timer_wrap text-center">
      <h6>Event starts on</h6>
      <p style="font-size:24px">3 - 4 Dec 2018 At The Transcorp Hilton Abuja</p>
      <p></p>
    </div>
  </div>
  <div class="col-sm-6 col-md-6 secondary-bg vc_col time-back">
    <div class="timer_wrap text-center">
      <div class="timer">
        <h6>Time Left To Conference</h6>
        {{--<div class="count" style=""></div>--}}
        <div class="countdown styled"></div>
      </div>
    </div>
  </div>
  
</section>
  <!-- /Banners Tagline -->
  <div class="dark-layer" style="opacity:0.5"></div>
</section>
<!-- /Banners --> 

<!--Timer Countdown-->

<!--/Time Countdown--> 

<!--  About Section -->
<section id="about" class="section-padding">
  <div class="container">
    <div class="row"> 
      <!-- Heading -->
      <div class="col-md-12">
        <div class="heading-sec">
          <div class="section-header text-center">
            <h2>Conference Overview</h2>
            <p>About the Conference </p>
          </div>
        </div>
      </div>
      <!-- /Heading --> 
    </div>
    <div class="row">
      <div class="col-md-7 col-lg-6">
        <div class="about-m-content">
          <p>The Conference brings together eminent persons, policy makers/public sector representatives, researchers, legal professionals, commerce and industry experts in ongoing conversations and exploration of issues, ideas and analysis of its core areas of concern, which are governance, law and development.</p>
          <p>JOAC will put front and centre, the daunting issue of governance, both at the governmental and corporate levels; the need to improve fiscal discipline especially in the area of public financial management systems; and improve human development index, that will result in prosperity for all.</p>
          <h5>Conference Objectives</h5>
          <ul >
            <li>Inevitability of instituting stronger governance and accountability at public and private sector levels, political party organizations, and the professions.</li>
            <li>Seek new ways of ensuring strict financial and fiscal discipline especially in the area of public financial management 
              systems as part of the strategy for checking waste and corruption.</p></li>
            <li>Working side by side with government and key stakeholders, the Conference will also seek to strengthen efforts to 
              diversify the economy, improve the business environment to attract new investments across sectors that will deliver 
              industrial development for increased economic growth and overall national GDP.</li>
            <li>The Event ultimately aims to find a pathway to achieving peace, security, and political stability, for sustainable 
              economic advancement and improved human development index.</li>

          </ul>
        </div>
      </div>
      <div class="col-md-5 col-lg-6" style="background:grey">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-6 abut-event-img"> <img src="{{('assets/images/hilton.png')}}" alt="image" height="259px"> </div>
          <div class="col-md-6 col-sm-6 col-xs-6 abut-event-img"> <img src="{{('assets/images/transcorp.jpg')}}" alt="image" height="259px"> </div>
          <div class="col-md-6 col-sm-6 col-xs-6 abut-event-img"> <img src="{{('assets/images/conf3.jpg')}}" alt="image" height="259px"> </div>
          <div class="col-md-6 col-sm-6 col-xs-6 abut-event-img"> <img src="{{('assets/images/venue.png')}}" alt="image" height="259px"> </div>
        </div>
      </div>
    </div>
    <div class="row pull-right" style="background:red">
      <div class="col-md-7 col-lg-6" >
        <p>wwwwwwwwwwwwwww</p>
      </div>
    </div>
  </div>
</section>
<!-- / About Section --> 

<!--  Conference info -->
<section class="our-conference section-padding parallex-bg">
  <div class="container">
    <div class="row">
      <div class="conference-m-box div_zindex">
        <div class="conf-icon-m">
          <div class="col-sm-4 col-xs-6">
            <div class="spekers-tickets-info"> <i class="fa fa-microphone"></i> <span class="summary">8 Speakers</span> </div>
          </div>
          
          <div class="col-sm-4 col-xs-6">
            <div class="spekers-tickets-info"> <i class="fa fa-flag"></i> <span class="summary">500+ Seats</span> </div>
          </div>
          <div class="col-sm-4 col-xs-6">
            <div class="spekers-tickets-info"> <i class="fa fa-calendar"></i> <span class="summary">2 Days Event</span> </div>
          </div>
        </div>
        <div class="resever-today text-center">
          <h2>Reserve Your Seat Today</h2>
          <a href="#" class="btn" data-toggle="modal" data-target="#registration_form" >Book Now</a> </div>
      </div>
    </div>
  </div>
  <div class="dark-layer"></div>
</section>
<!-- / Conference info --> 

<!--Conference Gallery-->
{{--<section class="section-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading-sec clearfix">
          <div class="section-header text-center">
            <h2>Successful Event Last 10 Years</h2>
            <p>Last Year Event Gallery</p>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Event Gallery -->
    <div id="gallery_slider" class="owl-carousel">
      <div class="item"> <img src="assets/images/event-slider-img-1.jpg" alt=""/> </div>
      <div class="item"><img src="assets/images/event-slider-img-2.jpg" alt=""/> </div>
      <div class="item"> <img src="assets/images/event-slider-img-3.jpg" alt=""/> </div>
      <div class="item"> <img src="assets/images/event-slider-img-1.jpg" alt=""/> </div>
      <div class="item"> <img src="assets/images/event-slider-img-2.jpg" alt=""/> </div>
      <div class="item"> <img src="assets/images/event-slider-img-3.jpg" alt=""/> </div>
    </div>
    <!-- /Event Gallery --> 
    
  </div>
</section>--}}
<!--/Conference Gallery--> 

<!--Conference Schedule-->

<!--Conference Schedule--> 

<!--Conference Speakers-->


<!--Conference-Speakers-Style-2-->

<!--/Conference Speakers--> 

<!--Conference Pricing-->

<!--/Conference Pricing--> 

<!--Conference Testimonial-->
<section id="testimonial" class="section-padding">
  <div class="container">
    <div class="row"> 
      <!-- Heading -->
      <div class="col-md-12">
        <div class="heading-sec">
          <div class="section-header text-center">
            <h2>Testimonials</h2>
            <p>Our Testimonial</p>
          </div>
        </div>
      </div>
      <!-- /Heading -->
      
      <div id="testimonial_slider" class="owl-carousel">
        
        <div class="item">
          <div class="testimonial_head">
            <div class="testimonial_img"> <img src="assets/images/gani.png" alt="image"> </div>
            <h5>LATE CHEIF GANI FAWEHINMI S.A.N</h5>
            <small>Legal practitioner and Human Right Activist</small> </div>
          <p>Justice Oputa is simply wonderful, he understands law in the most profound manner. He propagates law in the most legendary manner... I know him as Nigeria's Lord Denning; the exemplary Lord Denning, that is Justice Oputa.</p>
        </div>
        
        <div class="item">
          <div class="testimonial_head">
            <div class="testimonial_img"> <img src="assets/images/uche.png" alt="image"> </div>
            <h5>PROFESSOR UCHE UKO UCHE</h5>
            <small>Renowned Professor of Law</small> </div>
          <p>Justice Oputa ... Has made each imprint indelible; he has made each mark set on marble cast. He made them rather effortlessly without throwing weight around. He made them with effectiveness, yet without what we now describe as a regular mode of behavior, namely corruption.</p>
        </div>
        <div class="item">
          <div class="testimonial_head">
            <div class="testimonial_img"> <img src="assets/images/ike.png" alt="image"> </div>
            <h5>MAJ-GEN. IKE NWACHUKWU (RTD.)</h5>
            <small>Senator and former Minister of the Federal Government of Nigeria.</small> </div>
          <p>Justice Oputa is a highly intellectual person. He is in my view forthright, and has delivered some of the best judgements in the history of this country's judiciary.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/Conference Testimonial--> 

<!--Conference FAQ-->
<!--/Conference FAQ--> 

<!--Conference Sponsors-->
{{--<section id="sponsors" class="sponsors_style3 section-padding">
        <div class="container">
          <div class="row"> 
            <!-- Heading -->
            <div class="col-md-12">
              <div class="heading-sec">
                <div class="section-header text-center">
                  <h2>Sponsors Style 3</h2>
                  <p>About Our Sponsors</p>
                </div>
              </div>
            </div>
            <!-- /Heading -->
      
            <div class="col-md-12">
                <div id="sponsors_table" class="table-responsive">
                  <table>
                      <tbody><tr>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_1.png" alt="image"></a></td>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_3.png" alt="image"></a></td>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                      </tr>
                      <tr>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_3.png" alt="image"></a></td>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_4.png" alt="image"></a></td>
                          <td><a href="#"><img src="sponsors2_files/sponsors_logo_1.png" alt="image"></a></td>
                      </tr>
                  </tbody></table>
              </div>
            </div>
            
          </div>
        </div>
      </section>--}}
<!--/Conference Sponsors--> 

<!--Become Sponsors--> 
{{--<section id="become_sponsor" class="section-padding text-center">
  <div class="container">
  	<div class="white_text div_zindex">
  		<h2>Become a Sponsor</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        <div class="space-30"></div>
	    <a href="#" class="btn margin-btm-20">Download PDF</a>
	    <a href="#" class="btn margin-btm-20">Get Started Today</a>
    </div>
  </div>
  <div class="dark-layer"></div>
</section>--}}
<!--/Become Sponsors--> 

  
<!-- Blog--> 
<section id="blog" class="section-padding">
  <div class="container">
    <div class="row">
      <!-- Heading--> 
      <div class="col-md-12">
          <div class="heading-sec clearfix">
            <div class="section-header text-center">
              <h2>Event News</h2>
              <p>Latest Updates</p>
            </div>
          </div>
        </div>
      <!-- /Heading -->
      @foreach($post as $p)
        <div class="col-md-4 col-sm-4">
          <div class="blog_wrap">
            <div class="blog_img margin-btm-20">
                  <a href="{{ route('show',['slug' => $p->slug]) }}"><img src="{{asset($p->image)}}" alt="image"></a>
              </div>
            <div class="blog_meta">
                  <p>{{$p->created_at->diffForHumans()}}</p>
              </div>
            <h5><a href="{{ route('show',['slug' => $p->slug]) }}">{{$p->title}}</a></h5>
            <p>{{substr($p->content,0,400)}}</p>
          </div>
        </div>
      @endforeach
      
</section>
<!-- /Blog --> 

<!--Conference Venue-->
<section id="venue" class="section-padding parallex-bg white_text">
  <div class="container">
    <div class="div_zindex">
      <div class="row">
        <div class="col-md-12">
          <div class="heading-sec clearfix">
            <div class="section-header text-center">
              <h2>Conference Venue</h2>
              <p>Transcorp Hilton Abuja</p>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-md-4">
          <div class="venue_info_box"> <i class="fa fa-phone" aria-hidden="true"></i>
            <p>+234-816-372-7415</p>
            <p>+234-803-302-1661</p>
          </div>
        </div>
        <div class="col-sm-4 col-md-4">
          <div class="venue_info_box"> <i class="fa fa-map-marker" aria-hidden="true"></i>
            <p>1 Aguiyi Ironsi St, Wuse 900001, Maitama</p>
          </div>
        </div>
        <div class="col-sm-4 col-md-4">
          <div class="venue_info_box"> <i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:support@domainname.com">support@oputaconference.org</a> <a href="mailto:admin@oputaconference.org">admin@oputaconference.org</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="dark-layer"></div>
</section>
<!--/Conference Venue--> 

<!--map-->
<div id="map">
  <div id="js-gmap" class="gmap js-gmap"></div>
</div>
<!--map--> 

<!--Newsletter 
<section id="newsletter">
<div class="container">
	<h4>Subscribe to Newsletter</h4>
    <iframe src="newsletter.php" class="newslette-iframe" scrolling="no"></iframe>
</div>
</section>
<!--/Newsletter--> 
            
<!--Footer-->
<footer class="secondary-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-push-4 text-center"> 
      	<div class="footer_logo">
            <h3><a href="#"><span>JO</span>AC</a></h3>
        </div>
      </div>
      <div class="col-md-4 col-md-push-4">
        <ul class="social_links">
          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-md-pull-8">
        <p>&copy; 2018 All Rights Reserved.</p><br>
        <small>Powered By <a href="#">AppNov Tech</a></small>
      </div>
    </div>
  </div>
</footer>
<!--/footer--> 
<!--Back to top-->
<div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>

<div id="registration_form" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content padding_4x4_40 text-center">
      <div class="modal-header">
        <h3>Reserve Your Seat</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form id="js-ajax-form" class="js-ajax-form">
        <div class="form-group">
          <input class="form-control" name="name" type="text" placeholder="Full Name" required="">
        </div>
        <div class="form-group">
          <input class="form-control" name="email" type="email" placeholder="Email Address" required="">
        </div>
        <div class="form-group">
          <input class="form-control" name="phone" type="text" placeholder="Phone Number" required="">
        </div>
        <div class="form-group">
          <select class="form-control" name="Numberofseats" required="">
            <option value="">Number of Seats</option>
            <option value="1">1 Seat</option>
            <option value="2">2 Seats</option>
            <option value="3">3 Seats</option>
            <option value="4">4 Seats</option>
            <option value="5">5 Seats</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control" name="eventPlan" required="">
            <option value="">Choose a Plan </option>
            <option value="FrontSeat">Front Seat</option>
            <option value="MiddleSeat">Middle Seat</option>
            <option value="BackSeat">Back Seat</option>
            <option value="VIP">VIP</option>
          </select>
        </div>
        <div class="form-group">
          <input class="btn" name="register-btn" type="submit" value="Register Now!">
        </div>
      </form>
    </div>
  </div>
</div>
<!--/Back to top--> 

<!-- Scripts --> 
<script src="{{asset('assets/js/jquery.min.js')}}"></script> 
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script> 

<script src="http://maps.google.com/maps/api/js?sensor=true"></script> 
<script src="{{asset('assets/js/gmaps.min.js')}}"></script> 
<script src="{{asset('assets/js/jquery.validate.min.js')}}"></script> 
<script src="{{asset('assets/js/interface.js')}}"></script> 
<!-- testimonial-slider-JS--> 
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script> 
<!--Switcher-->
<script src="{{asset('assets/switcher/js/switcher.js')}}"></script>
<!-- Countdown--> 
<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
<!-- prettyPhoto--> 
<script src="{{asset('assets/js/jquery.prettyPhoto.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('assets/js/aos.js')}}" type="text/javascript" charset="utf-8"></script>
<script>
  AOS.init({
  duration: 1200,
  easing: 'ease-in-out-sine'
  })
</script>
</body>
</html>
