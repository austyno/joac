@extends('pages.layout.main')
<style>
.speaker-info {
	margin-bottom:5px;
	padding:15px 0;
}
.speaker-info h5 {
	margin-bottom:0px;
}
.speaker-info h6 {
	font-size:16px;
	font-weight:300;
	line-height:32px;
}
.speaker-info-box {
	position:relative;
}
.social-icons a {
	color:#ffffff;
	font-size:40px;
	line-height:100%;
	margin-left:21px;
}
.social-icons {
	position:absolute;
	top:50%;
	transform:translateY(-50%);
	width:100%;
}
.speaker-info-box:hover .speaker-hover {
	opacity:.7;
}
.speaker-hover {
	background:silver none repeat scroll 0 0;
	height:100%;
	left:0;
	opacity:0;
	position:absolute;
	right:30px;
	top:0;
	transition:all 0.5s linear 0s;
	vertical-align:middle;
	width:100%;
}
.social-icons a > i {
  font-size: 30px;
}
.speaker-sec {
  margin: 0 auto;
  max-width: 460px;
}
.border-box {
	border:12px solid #cccccc;
    overflow:hidden;
}
.speaker-info-box {
	margin:0 auto;
}
.speaker-info {
	margin-bottom:10px;
	padding:10px 0;
}
.spearker-img img {
  width: 100%;
  height:350px;
}
h5 a small{
    font-size:13px;
}
.speaker-info h6{
    font-size:20px;
    color:#118ec6
}
</style>

@section('content')
<section id="banner" style="height:150px"> 
        <div class="dark-layer" style="opacity:0.8"></div>
</section>

<section id="speakers" class="spearker-section section-padding">
        <div class="container">
          <div class="row"> 
            <!-- Heading -->
            <div class="col-md-12">
              <div class="heading-sec">
                <div class="section-header text-center">
                  <h2>Board Members</h2>
                  
                </div>
              </div>
            </div>
            <!-- /Heading --> 
          </div>
          <div class="row" data-aos="fade-right"> 
            <!-- speakers -->
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/karibi2.png')}}" alt="" class="img-responsive center-block" > </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Rtd Justice of the Supreme Court, Chairman BOT</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6 style="">Rtd Justice Adolphus Karibi Whyte<small style="font-size:13px">CON,GCON, JSC.(rtd) LL.B, LL.M, Ph.D</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/bish.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Bishop Hassan Matthew Kukah<small>Ph.D, Member, Investigation Commission of Human Rights Violations</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/senetor.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Senator Ben Obi<small>Senator of the Federal Republic</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/george.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Executive Director</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Bar George Oputa Esq<small>LL.B, BL</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
        </div>
        <div class="row" data-aos="fade-right"> 
        
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/akanbi.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p></div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Prof. Mustapha M. Akanbi<small>SAN,Former Dean, School of Law, Unilorin.</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/osagie.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Dr. Phil Osagie<small>Ph.D, Businessman, Global Communications Expert.</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/prof.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Prof. Charles Q. Dokubo<small>Special Assistant to the President on Niger Delta and Coordinator of the Amnesty Programme</small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{('assets/images/chalieboy.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Mr. Charles  Oputa<small>Entertainer, Entrepreneur </small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <!-- /speakers --> 
        </div>
        <div class="row" data-aos="fade-right">
                <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="speaker-sec">
                          <div class="speaker-info-box text-center border-box">
                            <div class="spearker-img"> <img src="{{('assets/images/ejembi.png')}}"  alt="" class="img-responsive center-block"> </div>
                            <div class="speaker-hover">
                              <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Board Member</p> </div>
                            </div>
                          </div>
                          <div class="speaker-info">
                            <h6>Mrs. Mary Udu-Ejemb<small>Director in the service of the Federal Republic of Nigeria.</small></h6>
                            <h6></h6>
                          </div>
                        </div>
                      </div>

        </div>
     </div>
      </section>
      <!-- /Speakers-1 --> 

@endsection