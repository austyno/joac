@extends('pages.layout.main')

@section('content')
<section id="banner" style="height:150px"> 
        <div class="dark-layer" style="opacity:0.8"></div>
</section>


<!--Conference-Speakers-Style-2-->
<section id="our_speakers" class="speakers_style2 section-padding">
    <div class="container">
      <div class="row"> 
        <!-- Heading -->
        <div class="col-md-12">
          <div class="heading-sec">
            <div class="section-header text-center">
              <h2>The Team</h2>
              <div class="divider double"></div><br>
              <h5><u><i>Honourary Patrons</i></u></h5>
            </div>
          </div>
        </div>
        <!-- /Heading -->

          <!-- Speakers-2 -->
          <div class="col-sm-4 col-md-6">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{('assets/images/obj.png')}}" alt="image" height="300px" width="300px"> 
                      <div class="speakers_follow_us">
                      <ul>
                          <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                      </ul>
                  </div>
                  </div>
                  <div class="speakers_info">
                      <h5>Chief Olusegun Obasanjo <small style="font-size:12px">GCFR,Ph.D.,Former President,Federal Republic of Nigeria(1999 to 2007). </small></h5>
                      
                      <p></p>
                      Honourary Patron Justice Oputa Foundation.
                  </div>
                  
              </div>
          </div>
          
          <!-- Speakers-3 -->
          <div class="col-sm-4 col-md-6">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{('assets/images/obi.png')}}" alt="image" height="269px" width="300px"> 
                      <div class="speakers_follow_us">
                      <ul>
                          <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                      </ul>
                  </div>
                  </div>
                  <div class="speakers_info">
                      <h5>HRM Nnaemeka Alfred Nnanyelugo Achebe</h5>
                      <small>CFR, mni. Honourary Patron Justice Oputa Foundation.</small>
                      <p>Obi of Onitsha, presently Chancellor, Ahmadu Bello University since 2015</p>
                  </div>
                  
              </div>
          </div>
      </div>

      <div class="divider double"></div><br>

      <div class="row">
        <div class="row"> 
            <!-- Heading -->
            <div class="col-md-12">
              <div class="heading-sec">
                <div class="section-header text-center">
                  <h5><u><i>Board Members</i></u></h5>
                  <p></p>
                </div>
              </div>
            </div>
            <!-- /Heading -->
    
              <!-- Speakers-2 -->
              <div class="col-sm-4 col-md-4">
                  <div class="speakers_wrap">
                      <div class="our_speaker_img"> 
                          <img src="{{('assets/images/karibi.png')}}" alt="image" height="250px" width="300px"> 
                          <div class="speakers_follow_us">
                          <ul>
                              <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                              <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                          </ul>
                      </div>
                      </div>
                      <div class="speakers_info">
                          <h5>Rtd Justice Adolphus Karibi Whyte</h5>
                          <small>CON, JSC.(rtd) LL.B, LL.M, Ph.D, GCON</small>
                          <p>Chair, BOT/Justice Oputa Foundation</p>
                      </div>
                      
                  </div>
              </div>

              <div class="col-sm-4 col-md-4">
                <div class="speakers_wrap">
                    <div class="our_speaker_img"> 
                        <img src="{{('assets/images/bish.png')}}" alt="image" height="250px" width="300px"> 
                        <div class="speakers_follow_us">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="speakers_info">
                        <h5>Bishop Hassan Matthew Kukah</h5>
                        <small>PhD.</small>
                        <p>Board Member, Justice Oputa Foundation</p>
                    </div>
                    
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="speakers_wrap">
                    <div class="our_speaker_img"> 
                        <img src="{{('assets/images/senetor.png')}}" alt="image" height=283px" width="300px"> 
                        <div class="speakers_follow_us">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="speakers_info">
                        <h5>Senator Ben Obi</h5>
                        <small>Senator of the Federal Republic</small>
                        <p>Board Member, Justice Oputa Foundation</p>
                    </div>
                    
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="speakers_wrap">
                    <div class="our_speaker_img"> 
                        <img src="{{('assets/images/george.png')}}" alt="image" height="250px" width="300px"> 
                        <div class="speakers_follow_us">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="speakers_info">
                        <h5>George Oputa Esq</h5>
                        <small>LL.B, BL</small>
                        <p>Executive Director.</p>
                    </div>
                    
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="speakers_wrap">
                    <div class="our_speaker_img"> 
                        <img src="{{('assets/images/akanbi.png')}}" alt="image" height="220px" width="300px"> 
                        <div class="speakers_follow_us">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="speakers_info">
                        <h5>Prof. Mustapha M. Akanbi</h5>
                        <small>SAN</small>
                        <p>Former Dean, School of Law, Unilorin.</p>
                    </div>
                    
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="speakers_wrap">
                    <div class="our_speaker_img"> 
                        <img src="{{('assets/images/osagie.png')}}" alt="image" height="250px" width="300px"> 
                        <div class="speakers_follow_us">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    </div>
                    <div class="speakers_info">
                        <h5>Dr. Phil Osagie</h5>
                        <small>PhD</small>
                        <p>Businessman, Global Communications Expert.</p>
                    </div>
                    
                </div>
            </div>

          </div>
      </div>

      <div class="row">
        <div class="col-sm-4 col-md-4">
            <div class="speakers_wrap">
                <div class="our_speaker_img"> 
                    <img src="{{('assets/images/prof.png')}}" alt="image" height="250px" width="300px"> 
                    <div class="speakers_follow_us">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                </div>
                <div class="speakers_info">
                    <h5>Prof. Charles Q. Dokubo</h5>
                    <small>Special Assistant to the President on Niger Delta and Coordinator of the Amnesty Programme.</small>
                    <p></p>
                </div>
                
            </div>
        </div>

        <div class="col-sm-4 col-md-4">
            <div class="speakers_wrap">
                <div class="our_speaker_img"> 
                    <img src="{{('assets/images/chalieboy.png')}}" alt="image" height="300px" width="400px" style="margin:auto"> 
                    <div class="speakers_follow_us">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                </div>
                <div class="speakers_info">
                    <h5>Mr. Charles  Oputa</h5>
                    <small>Entertainer, Entrepreneur </small>
                    <p></p>
                </div>
                
            </div>
        </div> 
          <!-- Speakers-3 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{('assets/images/ejembi.png')}}" alt="image" height="282px" width="300px"> 
                      <div class="speakers_follow_us">
                      <ul>
                          <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                          <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                      </ul>
                  </div>
                  </div>
                  <div class="speakers_info">
                      <h5>Mrs. Mary Udu-Ejemb</h5>
                      <small>Director in the service of the Federal Republic of Nigeria.</small>
                      <p></p>
                  </div>
                  
              </div>
          </div>
      </div>
    </div>   


  </section>
  <div class="divider double"></div>

  <section id="our_speakers" class="speakers_style3 section-padding">
    <div class="container">
      <div class="row"> 
        <!-- Heading -->
        <div class="col-md-12">
          <div class="heading-sec">
            <div class="section-header text-center">
              <h5><u><i>Conference Planning Committee</i></u></h5>
              
            </div>
          </div>
        </div>
        <!-- /Heading -->
        
            <!-- Speakers-1 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/bassey.png')}}" alt="image"> 
                  </div>
                  <div class="speakers_info">
                      <h5>Bassey Essien</h5>
                      <small>JOAC Event Consultant/member event planning and sponsorship sub-committee</small>
                  </div>
              </div>
          </div>
          
          <!-- Speakers-2 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/george.png')}}" alt="image"> 
                  </div>
                  <div class="speakers_info">
                      <h5>George Oputa Esq.</h5>
                      <small>Member,Sponsorship & Fund Raising Sub-Committee</small>
                  </div>
              </div>
          </div>
          
          <!-- Speakers-3 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/ejembi.png')}}" alt="image"> 
                  </div>
                  <div class="speakers_info">
                      <h5> Mary Udu-Ejembi </h5>
                      <small>JOF Board Secretary Member, Media & Publicity Sub-Committee</small>
                  </div>
              </div>
          </div>
          
          <!-- Speakers-4 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/samson.png')}}" alt="image"> 
                  </div>
                  <div class="speakers_info">
                      <h5>Hon Samson Osagie</h5>
                      <small>Member, Budget & Finance Sub-Committee</small>
                  </div>
              </div>
          </div>
          
          <!-- Speakers-5 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/tam.png')}}" alt="image"> 
                  </div>
                  <div class="speakers_info">
                      <h5>Dr. Austin Tam George</h5>
                      <small>Member, Programming Sub-Committee</small>
                  </div>
              </div>
          </div>
          
          <!-- Speakers-6 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/austyno.png')}}" alt="image"> 
                  </div>
                  <div class="speakers_info">
                      <h5>Austin Alozie</h5>
                      <small>Member, Logistics Sub-Committee</small>
                  </div>
              </div>
          </div>
      </div>

      <div class="row">

            <div class="col-sm-4 col-md-4">
                    <div class="speakers_wrap">
                        <div class="our_speaker_img"> 
                            <img src="{{asset('assets/images/chima.png')}}" alt="image"> 
                        </div>
                        <div class="speakers_info">
                            <h5>Hon. Chimamkpam Anyamkpa.</h5>
                            <small>Member Sponsorship & Fund Raising Committee</small>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                        <div class="speakers_wrap">
                            <div class="our_speaker_img"> 
                                <img src="{{asset('assets/images/frank.png')}}" alt="image"> 
                            </div>
                            <div class="speakers_info">
                                <h5>Frank Onuah.</h5>
                                <small>Member Programmes Committee</small>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-md-4">
                            <div class="speakers_wrap">
                                <div class="our_speaker_img"> 
                                    <img src="{{asset('assets/images/omo.png')}}" alt="image"> 
                                </div>
                                <div class="speakers_info">
                                    <h5>Mariel Rae-Omo.</h5>
                                    <small>Member, Logistics, Media & Sponsorship Committees</small>
                                </div>
                            </div>
                        </div>

      </div>

<div class="row">
        <div class="col-sm-4 col-md-4">
                <div class="speakers_wrap">
                    <div class="our_speaker_img"> 
                        <img src="{{asset('assets/images/denis.png')}}" alt="image"> 
                    </div>
                    <div class="speakers_info">
                        <h5>Denis Goje.</h5>
                        <small>Member, Logistics Committee</small>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                    <div class="speakers_wrap">
                        <div class="our_speaker_img"> 
                            <img src="{{asset('assets/images/otaru.png')}}" alt="image"> 
                        </div>
                        <div class="speakers_info">
                            <h5>Florence Otaru.</h5>
                            <small>Member, Budget & Finance Sub-Committee</small>
                        </div>
                    </div>
                </div>
</div>

    </div>
  </section>

  


@endsection