@extends('pages.layout.main')

@section('content')



<section id="pricing" class="pricing_style3 section-padding parallex-bg white_text parallex-bg" style="background-image:url(../assets/images/conf.jpg);">
    <div class="container">
      <div class="div_zindex">
        <div class="row">
          <div class="col-md-12">
            <div class="heading-sec clearfix">
              <div class="section-header text-center">
                <h2>Pricing</h2>
                <p>Buy Your Ticket</p>
              </div>
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="event_priceing">
                <h5>Business Rate </h5>
              <p></p>
               <div class="plan_price">
                    <p>N250,000.00 for 5 Persons</p>
                    <button class="btn" data-toggle="modal" data-target="#regform">Register Now</button>
                </div>
              </div>
          </div>
          
          <div class="col-md-6">
            <div class="event_priceing">
                <h5>Government Rate </h5>
              <p></p>
               <div class="plan_price">
                    <p>N200,000.00 for 5 Persons</p>
                    <button class="btn" data-toggle="modal" data-target="#regform">Register Now</button>
                </div>
              </div>
          </div>
          
          <div class="col-md-6">
            <div class="event_priceing">
                <h5>Legal Practitioners</h5>
              <p></p>
               <div class="plan_price">
                    <p>N30,000.00 Per Person</p>
                    <button class="btn" data-toggle="modal" data-target="#regform">Register Now</button>
                </div>
              </div>
          </div>
          
          <div class="col-md-6">
            <div class="event_priceing">
                <h5>Serving Judges</h5>
              <p></p>
               <div class="plan_price">
                    <p>N50,000.00 Per Person</p>
                    <button class="btn" data-toggle="modal" data-target="#regform">Register Now</button>
                </div>
              </div>
          </div>

          <div class="col-md-6">
              <div class="event_priceing">
                  <h5>Retired Judges</h5>
                <p></p>
                 <div class="plan_price">
                      <p>N20,000.00 Per Person</p>
                      <button class="btn" data-toggle="modal" data-target="#regform">Register Now</button>
                  </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="event_priceing">
                    <h5>Student Bodies</h5>
                  <p></p>
                   <div class="plan_price">
                        <p>N20,000.00 for 2 Persons</p>
                        <button class="btn" data-toggle="modal" data-target="#regform" data-plan="NGO">Register Now</button>
                    </div>
                  </div>
              </div>

              <div class="col-md-6">
                  <div class="event_priceing">
                      <h5>NGOs</h5>
                    <p></p><br>
                     <div class="plan_price">
                          <p>N50,000.00 for 2 Persons</p><br>
                           <button class="btn" data-toggle="modal" data-target="#regform" data-plan="NGO">Register Now</button>
                      </div>
                    </div>
                </div>
      
        </div>
      </div>
    </div>
  <div class="dark-layer"></div>   
  </section>
<!-- Registration Form -->
<div class="modal fade" id="regform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:80px">
    <div class="modal-dialog"> 
      <!-- Modal content-->
      <div class="modal-content padding_4x4_40 text-center">
        <div class="modal-header">
          <h3>Reserve Your Seat</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

      <form id="js-ajax-form">
          {{csrf_field()}}
          <div class="form-group">
            <input class="form-control" name="name" type="text" placeholder="Full Name" required="">
          </div>
          <div class="form-group">
            <input class="form-control" name="email" type="email" placeholder="Email Address" required="">
          </div>
          <div class="form-group">
            <input class="form-control" name="phone" type="text" placeholder="Phone Number" required="">
          </div>
          <div class="form-group">
            <select class="form-control" name="numberofseats" required="">
              <option value="">Number of Seats</option>
              <option value="1">1 Seat</option>
              <option value="2">2 Seats</option>
              <option value="3">3 Seats</option>
              <option value="4">4 Seats</option>
              <option value="5">5 Seats</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" name="eventPlan" required="">
              <option value="">Choose a Plan </option>
              <option value="business">Business Rate</option>
              <option value="retiredjudges">Retired Judges</option>
              <option value="servingjudges">Serving Judges</option>
              <option value="ngo">NGOs</option>
              <option value="students">Student Bodies</option>
              <option value="legal">Legal Practitioners</option>
              <option value="govt">Goverment Rate</option>
            </select>
          </div>
          <div class="form-group">
            <input class="btn" type="submit" value="Register Now!">
          </div>
        </form>

      </div>
    </div>
  </div>
  <!-- /Registration Form --> 
<!-- Registration-success-message -->
<div id="success" class="modal modal-message fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></span>
        <h2 class="modal-title">Thank You!</h2>
        <p class="modal-subtitle">Your message is successfully sent...</p>
      </div>
    </div>
  </div>
</div>
<!-- /Registration-success-message --> 

<!-- Registration-error -->
<div id="error" class="modal modal-message fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></span>
        <h2 class="modal-title">Sorry...</h2>
        <p class="modal-subtitle"> Something went wrong </p>
      </div>
    </div>
  </div>
</div>
<!-- /Registration-error --> 


@endsection