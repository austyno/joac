@extends('pages.layout.main')

@section('content')

<section id="banner" style="height:150px"> 
        <div class="dark-layer" style="opacity:0.8"></div>
</section>

<div class="container">
    <section class="section-padding" style="margin-left;" >
            <h5 style="margin-left:;margin:50px 0 50px 35%"><i>Why become a Sponsor</h5>
            <div class="row">
                    <div class="col-md-4 sponsor card mb-4 shadow-sm" data-aos="fade-up" style="padding-bottom:40px">
                            <i class="fa fa-book fa-3x "></i>
                             <p><i class="fa fa-long-arrow-right"></i> Increased brand awareness.</li></p>
                              <p><i class="fa fa-long-arrow-right"></i> Participants remember and recognize your company, products and features</li></p>
                              <p><i class="fa fa-long-arrow-right"></i> Participants see you as a supporter of public good</li></p>
                              <p><i class="fa fa-long-arrow-right"></i> See you as a partner and hold you above others in purchasing decisions</li></p>
                    </div>
                    <div class="col-md-4 sponsor mb-4 shadow-sm" data-aos="fade-up" data-aos-delay="700" style="">
                      <i class="fa fa-gavel fa-3x"></i>
                      <p><i class="fa fa-long-arrow-right"></i> Reach local and global audiences of senior decision makers, eminent persons, policy 
                        makers, business leaders, middle management and young professionals.</li></p>
                      <p><i class="fa fa-long-arrow-right"></i> Showcase your activities and expertise in this world class event to both public and private sectors, and  professionals from the legal and other industries</li></p>
                      
                    </div>
                    <div class="col-md-4 sponsor card mb-4 shadow-sm" data-aos="fade-up" data-aos-delay="1200" style="padding-bottom:50px">
                            <i class="fa fa-gift fa-3x"></i>
                            <p><i class="fa fa-long-arrow-right"></i>  Interact with key players of the polity, policy makers, and business in very interesting plenary sessions</li></p>
                            <p><i class="fa fa-long-arrow-right"></i> Have a voice in a specially arranged forum on ethics and best practices in government and business</li></p>
                            <p><i class="fa fa-long-arrow-right"></i> Stand out in the crowd and present as a power player in Abuja.</li></p> 
                            
                    </div>
                  </div>


                       
</section>

<section class="">

        <h5 class="text-center" style="color:#228dcb;">Sponsorship Packages</h5>
        <hr>
        
<div class="row">
    <div class="container">
        <div class="col-sm-12" style="" data-aos="fade-right" data-aos-delay="" >
            <div class="panel panel-default">
                <div class="panel-heading text-center" style=""><h5>Platinum N30 million</h5></div>
                <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                    <div class="well well-sm">Pre Event Involvement </div>
                        <ul>
                            <li>Logo on the front cover of the brochure under the second highest heading of Platinum Sponsor which will be mailed to JOAC contact database.</li>
                            <li>Company logo and profile featured on JOF website homepage as Platinum Sponsor </li>
                            <li>Company logo on the E-flyer (for promotion before October 2018.</li>
                            <li>Reciprocal website hyperlink to the company’s website on all e-materials </li>
                            <li>Acknowledgements in all PR and media campaigns where ever possible</li>
                        </ul>
                         
                    <div class="well well-sm">Onsite Branding and Exhibitions</div>
                    <ul>
                        <li>One Keynote speaking slot opportunity (CEO) in one plenary session /plus 1 Moderator opportunity in one plenary session </li>
                        <li>Platinum sponsor will host one day lunch with opportunity to brand the entire lunch area (provided by the sponsor). </li>
                        <li>Logo to predominantly feature on conference signage displayed on auditorium (back drop), through out conference venue (banners) as Platinum Sponsors </li>
                        <li>Recognition from the MC at the Welcome Reception and Opening Ceremony</li>
                        <li>18 sqm exhibition space of choice for networking</li>
                        <li>Logo on conference documentations as Platinum Sponsor </li>
                        <li>One Full Page dedicated for Sponsor’s company description on the Conference Booklet & Programme</li>
                        <li>Branded Delegate Bags with company logo/ organizers and conference logo to be given to each delegate upon registration</li>
                        <li>Sponsors’ corporate literature or promotional material in conference bags </li>
                        <li>Four complimentary full conference registrations, including tickets for the Patrons’s Gala dinner/three complimentary passes to company staff for the exhibition area</li>
                    </ul>
                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                </div>
              </div>
              
          </div>
          
    </div>
    
</div>


<div class="row">
        <div class="container">
            <div class="col-sm-12"  data-aos="fade-right">
                <div class="panel panel-default">
                       <div class="panel-heading text-center"><h5>Gold Sponsor  N20 million</h5></div>
                       <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                            <div class="well well-sm">Pre Event Involvement </div>
                            <ul>
                                <li>Logo on the front cover of the brochure under the heading of Gold Sponsor which will be mailed to JOAC database</li>
                                <li>Company logo on the flyer under the heading of Gold Sponsor</li>
                                <li> Company logo and profile featured on the website as Gold Sponsor </li>
                                <li>Company logo to feature on the E- brochure</li>
                                <li>Reciprocal website hot link to the company’s website.</li>
                                <li>Acknowledgements in all PR and media campaigns where ever possible</li>
                            </ul>
                            <div class="well well-sm">Onsite Branding and Exhibitions</div>
                            <ul>
                                <li>One chair person slot offered in the industry session </li>
                                <li>One presentation offered during the industry session </li>
                                <li> Logo to predominantly feature on congress signage displayed on auditorium (back drop), throughout the conference venue as Gold Sponsor. </li>
                                <li>One pop up corporate roll-up at the registration desk and exhibition area or refreshment area produced by organizer</li>
                                <li> One booth of 9 sqm exhibiting area offered for networking </li>
                                <li>Half Page dedicated for Sponsor’s advert in the final event program</li>
                                <li>Logo on conference documentations as Gold Sponsor </li>
                                <li>Corporate literature / promotional material to be included in the delegate pack</li>
                                <li>Two full complimentary conference registrations </li>
                                <li>Two complimentary passes for company staff in the exhibition area.</li>
                            </ul>
                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                       </div>
                </div>
            </div>
        </div>
            
                
    </div>

    <div class="row">
            <div class="container">
                <div class="col-sm-12"  data-aos="fade-right">
                    <div class="panel panel-default">
                           <div class="panel-heading text-center"><h5>Silver Sponsor  N10 million</h5></div>
                           <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                <div class="well well-sm">Pre Event Involvement </div>
                                <ul>
                                    <li>Logo on the front cover of the brochure under the heading of Silver Sponsor.</li>
                                    <li>Company logo on the conference flyer under the heading of Silver Sponsor.</li>
                                    <li> Company logo and profile featured on the website as Silver Sponsor </li>
                                    <li>Reciprocal website hyperlink to the company’s website.</li>
                                </ul>
                                <div class="well well-sm">Onsite Branding and Exhibitions</div>
                                <ul>
                                    <li>Logo to predominantly feature on congress signage displayed on auditorium (back drop), throughout conference venue as Silver Sponsor. </li>
                                    <li>One booth of 9 sqm exhibiting area offered for networking  </li>
                                    <li> Logo to predominantly feature on congress signage displayed on auditorium (back drop), throughout the conference venue as Gold Sponsor. </li>
                                    <li>Half Page dedicated for Sponsor’s advert on the final program </li>
                                    <li> One booth of 9 sqm exhibiting area offered for networking </li>
                                    <li>Logo on conference documentations as Silver Sponsor. </li>
                                    <li>1 complimentary full conference registrations .</li>
                                    <li>2 complimentary passes for the staff for exhibition area.</li>
                                </ul>
                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                           </div>
                    </div>
                </div>
            </div>
                
                    
        </div>

        <div class="row">
                <div class="container">
                    <div class="col-sm-12"  data-aos="fade-right">
                        <div class="panel panel-default">
                               <div class="panel-heading text-center"><h5>Patrons’ Gala Awards Dinner Sponsor   N10 million</h5></div>
                               <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                    <div class="well well-sm">Pre Event Involvement </div>
                                    <ul>
                                        <li>Company’s logo and profile to be featured on the conference website and booklet as “Support Sponsor”.</li>
                                        <li>Company logo and profile to be featured on event webpage as “Patron’s Gala Dinner Sponsor”.</li>
                                        <li> Reciprocal website hyperlink to your company’s home page from event.</li>
                                    </ul>
                                    <div class="well well-sm">Onsite Branding and Exhibitions</div>
                                    <ul>
                                        <li>Recognition by the MC at the opening of the Patrons’ Gala Dinner. </li>
                                        <li>Ten VIP Corporate Invitations to attend the Gala Dinner.</li>
                                        <li> Logo to predominantly feature on congress signage displayed on auditorium (back drop), throughout the conference venue as Gold Sponsor. </li>
                                        <li>Two sponsor branded roll ups to be produced by organizer.</li>
                                        <li> Additional branding opportunities (to be provided by sponsor upon approval of the organizer)</li>
                                        <li>Corporate literature and/or advertising material to be distributed at the Gala Dinner.</li>
                                        <li>All dinner menus on tables to be branded with sponsor’s logo.</li>
                                        
                                    </ul>
                                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                               </div>
                        </div>
                    </div>
                </div>                     
            </div>

            <div class="row">
                    <div class="container">
                        <div class="col-sm-12"  data-aos="fade-right">
                            <div class="panel panel-default">
                                   <div class="panel-heading text-center"><h5>Welcome Reception Sponsor  N10 million</h5></div>
                                   <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                        <div class="well well-sm">Pre Event Involvement </div>
                                        <ul>
                                            <li>Company’s logo and profile to be featured on the conference website and booklet as “Support Sponsor”.</li>
                                            <li>Sponsor to receive brochures for own distribution and promotional purposes.</li>
                                            <li> Company logo and profile to be featured on event website as “Welcome Reception Sponsor”.</li>
                                            <li>Reciprocal website hyperlink to company page from event website</li>
                                        </ul>
                                        <div class="well well-sm">Onsite Branding and Exhibitions</div>
                                        <ul>
                                            <li>Host sponsor of the “Welcome Reception Sponsor”  </li>
                                            <li> Inclusive of Banquet & Catering Costs (catering organized by the organizer).</li>
                                            <li> Name of company to be recognized at the Welcome Address. </li>
                                            <li>Sponsor entitled to 10 VIP Corporate Invitations to attend the Reception.</li>
                                            <li> Two Exclusive Branded Corporate roll ups produced by organizer.</li>
                                            <li>Possibility to have additional Branding Opportunities (to be provided by sponsors).</li>
                                            <li>Corporate Literature and/or Advertising Material to be distributed at Dinner Reception .</li>
                                            <li>All high tables to be branded with desk flags carrying the sponsor’s logo.</li>                                            
                                        </ul>
                                        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                                   </div>
                            </div>
                        </div>
                    </div>                     
                </div>



        <div class="row">
                <div class="container">
                    <div class="col-sm-12"  data-aos="fade-right">
                        <div class="panel panel-default">
                               <div class="panel-heading text-center"><h5>Delegate Bag Sponsor  N5 million</h5></div>
                               <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                    <div class="well well-sm">Pre Event Involvement </div>
                                    <ul>
                                        <li> Company’s logo and profile to be featured on the conference website and booklet as “Support Sponsor”. </li>           
                                    </ul>
                                    <div class="well well-sm">Onsite Branding and Exhibitions</div>
                                    <ul>
                                        <li>Name of company to be recognized at the Welcome Address. </li>
                                        <li>Four Exclusive Branded Corporate roll ups produced by organizer. </li>
                                        <li>Additional Branding Opportunities (to be provided by sponsors).</li>
                                        <li> Corporate Literature and/or Advertising Material to be distributed during the coffee breaks. </li>
                                        
                                    </ul>
                                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                               </div>
                        </div>
                    </div>
                </div>
                    
                        
            </div>

            <div class="row">
                    <div class="container">
                        <div class="col-sm-12"  data-aos="fade-right">
                            <div class="panel panel-default">
                                   <div class="panel-heading text-center"><h5>Lunch breaks Sponsor   N5 million</h5></div>
                                   <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                        <div class="well well-sm">Pre Event Involvement </div>
                                        <ul>
                                            <li> Company’s logo and profile to be featured on conference website and booklet as “Support Sponsor”.</li> 
                                            <li>Reciprocal website hyperlink to company home page from event website </li>         
                                        </ul>
                                        <div class="well well-sm">Onsite Branding and Exhibitions</div>
                                        <ul>
                                            <li>Name of company to be recognized by the MC at the welcome address and before each lunch break. </li>
                                            <li>Four Exclusive Branded Corporate roll ups produced by organizer. </li>
                                            <li>Additional Branding Opportunities (to be provided by sponsor).</li>
                                            <li> Other promotional options can be discussed with JOAC Secretariat. </li>
                                            
                                        </ul>
                                        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                                   </div>
                            </div>
                        </div>
                    </div>        
                            
                </div>




            <div class="row">
                    <div class="container">
                        <div class="col-sm-12"  data-aos="fade-right">
                            <div class="panel panel-default">
                                   <div class="panel-heading text-center"><h5>Coffee/Tea Break Sponsor  N2 million</h5></div>
                                   <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                        
                                        <ul>
                                            <li>Company’s logo and profile to be featured on the conference website and booklet as “Support Sponsor”. </li>
                                            <li>Sponsor to receive brochures for own distribution and promotional purposes.</li>
                                            <li> Company logo and profile to be featured on event website as “Support Sponsor”  </li>
                                            <li>Reciprocal website hyperlink to company home page from event website </li>
                                            <li>One branded Pop-Up Banner in the Conference Registration area highlighting the sponsorship.</li>
                                            <li>Branded Delegate Bags carrying company logo along with organizers and conference logo will be given to every delegate upon registration.</li>
                                            <li>Corporate Literature (company brochure) to be inserted in the delegate bag and given to every attendee.</li>
                                            <li>Networking/Exhibition space will be provided upon request at a subsidized rate.</li>
                                        </ul>
                                        <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                                   </div>
                            </div>
                        </div>
                    </div>        
                </div>


                <div class="row">
                        <div class="container">
                            <div class="col-sm-12"  data-aos="fade-right">
                                <div class="panel panel-default">
                                       <div class="panel-heading text-center"><h5>Bronze - In-Kind Contributor</h5></div>
                                       <div class="panel-body" style="padding:20px;justify-content:flex-start;font-family:Arial, Helvetica, sans-serif">
                                            
                                            <ul>
                                                <li>Contributions can include printing, commemorative souvenirs, communications equipment, office equipment and ground transportation. </li>
                                                <li>One full registration (includes access to all meals and events).</li>
                                                <li> Recognition as an In-Kind Contributor in Conference Program Book.</li>
                                                <li>If contribution totals N1,000,000.00 and over, company will be treated as a Event Sponsor  </li>
                                            </ul>
                                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#readmore"><i class="fa fa-angle-double-right"> Sponsor</i></button>
                                       </div>
                                </div>
                            </div>
                        </div>        
                    </div>
    </section>




    <div class="modal fade" id="readmore" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;padding:10px">
            <div class="modal-dialog">
             <div class="modal-content">
                <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <h5 id="dynamicName" class="modal-title" id="myModalLabel"></h5>
                   <div class="" id="image"></div>
                  </div>
                    <div class="modal-body">
    
                      <p> Contact Event Marketing Consultant for further discussion and sponsorship detail:</p> 

                            <p>Mr  Bassey Essien </p> 
                            <p>T: +2348163727415</p>
                            <p>W: <www class="justiceoputaconference org">www.justiceoputaconference,org</www></p>
                            
    
                      <div id="dynamicBio" style="height:auto"></div>
                       
                    </div>
                    <div class="modal-footer">
                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                       
                    </div>
                 </div><!-- /.modal-content -->
          </div><!-- /.modal -->
    
</div>

@endsection