@extends('pages.layout.main')
@section('content')
<section id="our_speakers" class="speakers_style2 section-padding">
    <div class="container">
      <div class="row"> 
        <!-- Heading -->
        <div class="col-md-12">
          <div class="heading-sec">
            <div class="section-header text-center">
              <h2>Speakers</h2>
              <p>Look Who's Speaking</p>
            </div>
          </div>
        </div>
        <!-- /Heading -->
        
            <!-- Speakers-1 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/obj.png')}}" alt="image" height="150px" width="200px">    
                  </div>
                  <div class="speakers_info">
                      <h5>Chief Olusegun Obasanjo</h5>
                      <small></small>
                      <p></p>
                  </div>
                  
              </div>
          </div>
          
          <!-- Speakers-2 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/obi.png')}}" alt="image" height="150px"> 
                  </div>
                  <div class="speakers_info">
                      <h5>HRM Nnaemeka Alfred Nnanyelugo Achebe</h5>
                      <small></small>
                      <p></p>
                  </div>
                  
              </div>
          </div>
          
          <!-- Speakers-3 -->
          <div class="col-sm-4 col-md-4">
              <div class="speakers_wrap">
                  <div class="our_speaker_img"> 
                      <img src="{{asset('assets/images/karibi.png')}}" alt="image" height="150px"> 
        
                  </div>
                  <div class="speakers_info">
                      <h5>Rtd Justice Adolphus Karibi Whyte</h5>
                      <small>Engineer at DigitalOcean</small>
                      <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum.</p>
                  </div>
                  
              </div>
          </div>
      </div>
    </div>
  </section>
  @endsection