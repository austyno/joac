@extends('pages.layout.main')

@section('content')
<section id="banner" style="height:150px"> 
  <div class="dark-layer" style="opacity:0.8"></div>
</section>

<section id="our_speakers" class="speakers_style2 section-padding">
    <div class="container">
      <div class="row"> 
        <!-- Heading -->
        <div class="col-md-12">
          <div class="heading-sec">
            <div class="section-header text-center">
              <h2>Speakers</h2>
              <p>Look Who's Speaking</p>
            </div>
          </div>
        </div>
        <!-- /Heading -->
        
        
        <div class="col-sm-6 col-md-6">
          <div class="speakers_wrap">
            <div class="our_speaker_img"> <img src="{{asset('assets/images/obj.png')}}" alt="image"></div>
            <div class="speakers_info">
              <h6>Chief Olusegun Obasanjo</h6> GCFR,Ph.D. Honourary Patron Justice Oputa Foundation.
              <small>Former President,Federal Republic of Nigeria(1999 to 2007).</small>
              <p></p>
            </div>
            <div class="speakers_follow_us">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="speakers_wrap">
            <div class="our_speaker_img"> <img src="{{asset('assets/images/obi.png')}}" alt="image" style="height:185px;">  </div>
            <div class="speakers_info">
              <h6>HRM Nnaemeka Alfred Nnanyelugo Achebe</h6>CFR, mni. Honourary Patron Justice Oputa Foundation. 
              <small>Obi of Onitsha, presently Chancellor, Ahmadu 
                Bello University since 2015</small>
              <p></p>
            </div>
            <div class="speakers_follow_us">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="speakers_wrap">
            <div class="our_speaker_img"> <img src="{{asset('assets/images/karibi.png')}}" alt="image" style="height:185px;"> </div>
            <div class="speakers_info">
              <h6>Rtd Justice Adolphus Karibi Whyte</h6>CON, JSC.(rtd) LL.B, LL.M, Ph.D, GCON
              <small>Chair, BOT/Justice Oputa Foundation</small>
              <p></p>
            </div>
            <div class="speakers_follow_us">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="speakers_wrap">
            <div class="our_speaker_img"> <img src="{{asset('assets/images/bish.png')}}" alt="image">  </div>
            <div class="speakers_info">
              <h6>Bishop Hassan Matthew Kukah</h6>PhD
              <small>Board Member, Justice Oputa Foundation</small>
              <p></p>
            </div>
            <div class="speakers_follow_us">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-6">
          <div class="speakers_wrap">
            <div class="our_speaker_img"> <img src="{{asset('assets/images/prof.png')}}" alt="image"> </div>
            <div class="speakers_info">
              <h6>Prof. Charles Q. Dokubo</h6>
              <small>Special Assistant to the President on Niger Delta and Coordinator of the Amnesty Programme.</small>
              <p></p>
            </div>
            <div class="speakers_follow_us">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-6">
          <div class="speakers_wrap">
            <div class="our_speaker_img"> <img src="{{asset('assets/images/akanbi.png')}}" alt="image"> </div>
            <div class="speakers_info">
              <h6>Prof. Mustapha M. Akanbi</h6>SAN. 
              <small>Former Dean, School of Law, Unilorin.</small>
              <p></p>
            </div>
            <div class="speakers_follow_us">
              <ul>
                <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>



<div class="row">
  <div class="col-sm-6 col-md-6">
    <div class="speakers_wrap">
      <div class="our_speaker_img"> <img src="{{asset('assets/images/george.png')}}" alt="image">  </div>
      <div class="speakers_info">
        <h6>George Oputa Esq</h6>LL.B, BL
        <small>Executive Director / CEO, Justice Oputa Foundation</small>
        <p></p>
      </div>
      <div class="speakers_follow_us">
        <ul>
          <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
  </div>


  <div class="col-sm-6 col-md-6">
    <div class="speakers_wrap">
      <div class="our_speaker_img"> <img src="{{asset('assets/images/tam.png')}}" alt="image">  </div>
      <div class="speakers_info">
        <h6>Dr. Tam Austin George</h6>
        <small>former Hon Commissioner for Information, Rivers State</small>
        <p></p>
      </div>
      <div class="speakers_follow_us">
        <ul>
          <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

    </div>
  </section>

@endsection