@extends('pages.layout.main')
<style>
.speaker-info {
	margin-bottom:5px;
	padding:15px 0;
}
.speaker-info h5 {
	margin-bottom:0px;
}
.speaker-info h6 {
	font-size:16px;
	font-weight:300;
	line-height:32px;
}
.speaker-info-box {
	position:relative;
}
.social-icons a {
	color:#ffffff;
	font-size:40px;
	line-height:100%;
	margin-left:21px;
}
.social-icons {
	position:absolute;
	top:50%;
	transform:translateY(-50%);
	width:100%;
}
.speaker-info-box:hover .speaker-hover {
	opacity:.7;
}
.speaker-hover {
	background:silver none repeat scroll 0 0;
	height:100%;
	left:0;
	opacity:0;
	position:absolute;
	right:30px;
	top:0;
	transition:all 0.5s linear 0s;
	vertical-align:middle;
	width:100%;
}
.social-icons a > i {
  font-size: 30px;
}
.speaker-sec {
  margin: 0 auto;
  max-width: 460px;
}
.border-box {
	border:12px solid #cccccc;
    overflow:hidden;
}
.speaker-info-box {
	margin:0 auto;
}
.speaker-info {
	margin-bottom:10px;
	padding:10px 0;
}
.spearker-img img {
  width: 100%;
  height:350px;
}
h5 a small{
    font-size:13px;
}
.speaker-info h6{
    font-size:20px;
    color:#118ec6
}
</style>

@section('content')
<section id="banner" style="height:150px"> 
        <div class="dark-layer" style="opacity:0.8"></div>
</section>

<section id="speakers" class="spearker-section section-padding">
        <div class="container">
          <div class="row"> 
            <!-- Heading -->
            <div class="col-md-12">
              <div class="heading-sec">
                <div class="section-header text-center">
                  <h2>Event Planning Team</h2>
                  
                </div>
              </div>
            </div>
            <!-- /Heading --> 
          </div>
          <div class="row" data-aos="fade-right"> 
            <!-- speakers -->
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/bassey.png')}}" alt="" class="img-responsive center-block" > </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Event Consultant</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6 style="">Mr. Bassey Essien<small style="font-size:13px"></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/george.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Sponsorship & Fund Raising</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Bar. George Oputa Esq.<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/ejembi.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Media & Publicity</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Mrs. Mary Udu-Ejembi<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/samson.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Budget & Finance Sub-Committee</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Hon Samson Osagie<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
        </div>
        <div class="row" data-aos="fade-right"> 
        
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/tam.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Programming Sub-Committee</p></div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Dr. Austin Tam George<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/austyno.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Media & Publicity Sub-Committee</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Mr. Austin Alozie<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/chima.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Sponsorship & Fund Raising Sub-Committee</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Hon. Chimamkpam Anyamkpa<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                  <div class="spearker-img"> <img src="{{asset('assets/images/frank.png')}}" alt="" class="img-responsive center-block"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Programmes Sub-Committee</p> </div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h6>Mr. Frank Onuah<small></small></h6>
                  <h6></h6>
                </div>
              </div>
            </div>
            <!-- /speakers --> 
        </div>
    <div class="row" data-aos="fade-right">
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box">
                <div class="spearker-img"> <img src="{{asset('assets/images/omo.png')}}"  alt="" class="img-responsive center-block"> </div>
                <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Logistics,Programmes, Media & Sponsorship Committees</p> </div>
                </div>
                </div>
                <div class="speaker-info">
                <h6>Mrs. Mariel Rae-Omo<small></small></h6>
                <h6></h6>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="speaker-sec">
                    <div class="speaker-info-box text-center border-box">
                    <div class="spearker-img"> <img src="{{asset('assets/images/cp.png')}}"  alt="" class="img-responsive center-block"> </div>
                    <div class="speaker-hover">
                        <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Logistic Sub-Committee</p> </div>
                    </div>
                    </div>
                    <div class="speaker-info">
                    <h6>CP Lawrence Alobi Esq<small></small></h6>
                    <h6></h6>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="speaker-sec">
                        <div class="speaker-info-box text-center border-box">
                        <div class="spearker-img"> <img src="{{asset('assets/images/eva.png')}}"  alt="" class="img-responsive center-block"> </div>
                        <div class="speaker-hover">
                            <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Media & Publicity/Programme</p> </div>
                        </div>
                        </div>
                        <div class="speaker-info">
                        <h6>Mrs. Eva Azinge Esq<small></small></h6>
                        <h6></h6>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="speaker-sec">
                            <div class="speaker-info-box text-center border-box">
                            <div class="spearker-img"> <img src="{{asset('assets/images/denis.png')}}"  alt="" class="img-responsive center-block"> </div>
                            <div class="speaker-hover">
                                <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Logistics Sub-Committees</p> </div>
                            </div>
                            </div>
                            <div class="speaker-info">
                            <h6>Mr. Denis Goje<small></small></h6>
                            <h6></h6>
                            </div>
                        </div>
                    </div>

    </div>
    <div class="row" data-aos="fade-right">
        <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="speaker-sec">
                    <div class="speaker-info-box text-center border-box">
                    <div class="spearker-img"> <img src="{{asset('assets/images/otaru.png')}}"  alt="" class="img-responsive center-block"> </div>
                    <div class="speaker-hover">
                        <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Budget & Finance Sub-Committee</p> </div>
                    </div>
                    </div>
                    <div class="speaker-info">
                    <h6>Mrs. Florence Otaru.<small></small></h6>
                    <h6></h6>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="speaker-sec">
                        <div class="speaker-info-box text-center border-box">
                        <div class="spearker-img"> <img src="{{asset('assets/images/roland.png')}}"  alt="" class="img-responsive center-block"> </div>
                        <div class="speaker-hover">
                            <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Logistics Sub-Committees</p> </div>
                        </div>
                        </div>
                        <div class="speaker-info">
                        <h6>Dr. Roland Aigbovo<small></small></h6>
                        <h6></h6>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="speaker-sec">
                            <div class="speaker-info-box text-center border-box">
                            <div class="spearker-img"> <img src="{{asset('assets/images/kuto.png')}}"  alt="" class="img-responsive center-block"> </div>
                            <div class="speaker-hover">
                                <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Budget & Finance</p> </div>
                            </div>
                            </div>
                            <div class="speaker-info">
                            <h6>Mr.Ernest Kuto<small></small></h6>
                            <h6></h6>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3">
                            <div class="speaker-sec">
                                <div class="speaker-info-box text-center border-box">
                                <div class="spearker-img"> <img src="{{asset('assets/images/alhaji.png')}}"  alt="" class="img-responsive center-block"> </div>
                                <div class="speaker-hover">
                                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">Member, Logistics Sub-Committees</p> </div>
                                </div>
                                </div>
                                <div class="speaker-info">
                                <h6>Alh. Abubakar Ali<small></small></h6>
                                <h6></h6>
                                </div>
                            </div>
                        </div>


    </div>
     </div>
      </section>
      <!-- /Speakers-1 --> 

@endsection