@extends('pages.layout.main')
<style>
.speaker-info {
	margin-bottom:5px;
	padding:15px 0;
}
.speaker-info h5 {
	margin-bottom:0px;
}
.speaker-info h6 {
	font-size:16px;
	font-weight:300;
	line-height:32px;
}
.speaker-info-box {
	position:relative;
}
.social-icons a {
	color:#ffffff;
	font-size:40px;
	line-height:100%;
	margin-left:21px;
}
.social-icons {
	position:absolute;
	top:50%;
	transform:translateY(-50%);
	width:100%;
}
.speaker-info-box:hover .speaker-hover {
	opacity:.7;
}
.speaker-hover {
	background:silver none repeat scroll 0 0;
	height:100%;
	left:0;
	opacity:0;
	position:absolute;
	right:30px;
	top:0;
	transition:all 0.5s linear 0s;
	vertical-align:middle;
	width:100%;
}
.social-icons a > i {
  font-size: 30px;
}
.speaker-sec {
  margin: 0 auto;
  max-width: 350px;
}
.img-border{
    border: 10px solid silver;
}

</style>
@section('content')
<section id="banner" style="height:150px"> 
        <div class="dark-layer" style="opacity:0.8"></div>
</section>

<!-- Speakers-1 -->
<section id="speakers" class="spearker-section section-padding">
        <div class="container">
          <div class="row"> 
            <!-- Heading -->
            <div class="col-md-12">
              <div class="heading-sec">
                <div class="section-header text-center">
                  <h2>Our Patrons</h2>
                  
                </div>
              </div>
            </div>
            <!-- /Heading --> 
          </div>
          <div class="row"> 
            <!-- speakers -->
            <div class="col-xs-12 col-sm-4 col-md-6">
              <div class="speaker-sec">
                <div class="speaker-info-box text-center border-box" style="">
                  <div class="spearker-img" > <img src="{{asset('assets/images/obi.png')}}" alt="" class="img-responsive img-border" style="height:350px"> </div>
                  <div class="speaker-hover">
                    <div class="social-icons text-center"> <p style="font-size:30px;color:#ffffff">HRM The Obi of Onitsha</p></div>
                  </div>
                </div>
                <div class="speaker-info">
                  <h5 style="font-size:24px"><a href="">HRM Nnaemeka Alfred Nnanyelugo Achebe<small style="font-size:13px">CFR, mni., Chancellor, Ahmadu Bello University since 2015</small></a></h5>
                  <h6></h6>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-6">
                    <div class="speaker-sec">
                      <div class="speaker-info-box text-center border-box" style="">
                        <div class="spearker-img" > <img src="{{asset('assets/images/obj.png')}}" alt="" class="img-responsive img-border" style="height:350px"> </div>
                        <div class="speaker-hover">
                          <div class="social-icons text-center"> <p ><h5 style="font-size:30px;color:#ffffff">President Ousegun Obasonjo</h5></p></div>
                        </div>
                      </div>
                      <div class="speaker-info">
                        <h5 style="font-size:24px"><a href="">Chief Ousegun Obasonjo<small style="font-size:13px">GCFR,Ph.D.,Former President,Federal Republic of Nigeria(1999 to 2007)</small></a></h5>
                        <h6></h6>
                      </div>
                    </div>
                  </div>
            
            <!-- /speakers --> 
          </div>
        </div>
      </section>

@endsection