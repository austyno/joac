@extends('layouts.app')

@section('content')
<link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }} <img src="{{ asset('assets/images/logo.jpg') }}" height="50px" style="opacity:.9;border-radius:18px;border:5px solid silver;float:right" /></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('custom') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                
                            </div>
                        </div>
                    </form>
                    <div style="backround-color:red">
                        <span class="font-size:100px">Login With</span>
                        <ul >
                            <li style="display:inline-block;list-style:none;"><a href="{{ route('socialaAuth',['provider'=>'facebook']) }}"><i class="fa fa-facebook fa-2x fa-border" aria-hidden="true"></i></a></li>
                            <li style="display:inline-block;list-style:none"><a href="{{ route('socialaAuth',['provider'=>'twitter']) }}"><i class="fa fa-twitter fa-2x fa-border" aria-hidden="true"></i></a></li>
                            <li style="display:inline-block;list-style:none"><a href="{{ route('socialaAuth',['provider'=>'linkedin']) }}"><i class="fa fa-linkedin fa-2x fa-border" aria-hidden="true"></i></a></li>
                            {{--<li style="display:inline-block;list-style:none"><a href="{{ route('socialaAuth',['provider'=>'github']) }}"><i class="fa fa-github-square fa-2x fa-border" aria-hidden="true"></i></a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
