<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'PagesController@index',
    'as' => 'index'
]);

Route::get('gallery',[
    'uses' => 'PagesController@gallery',
    'as' => 'gallery'
]);

Route::get('schedule',[
    'uses' => 'PagesController@schedule',
    'as' => 'schedule'
]);
Route::get('aboutus',[
    'uses' => 'PagesController@about',
    'as' => 'about'
]);
Route::get('speakers',[
    'uses' => 'PagesController@speakers',
    'as' => 'speakers'
]);
Route::get('pricing',[
    'uses' => 'PagesController@pricing',
    'as' => 'pricing'
]);
Route::get('sponsors',[
    'uses' => 'PagesController@sponsors',
    'as' => 'sponsors'
]);
Route::get('board',[
    'uses' => 'PagesController@board',
    'as' => 'board'
]);
Route::post('register',[
    'uses' => 'PagesController@register',
    'as' => 'register'
]);
Route::get('ajax/{id}',[
    'uses' => 'PagesController@ajax',
    'as' => 'ajax'
]);
Route::get('programs',[
    'uses' => 'PagesController@programs',
    'as' => 'programs'
]);
Route::get('Spnsorship',[
    'uses' => 'PagesController@Sponsorship',
    'as' => 'Spnsorship'
]);
Route::get('patrons',[
    'uses' => 'PagesController@patrons',
    'as' => 'patrons'
]);
Route::get('committee',[
    'uses' => 'PagesController@committee',
    'as' => 'committee'
]);

//blog
Route::get('blog',[
    'uses' => 'BlogController@index',
    'as' => 'blog'
]);
Route::get('single/{slug}',[
    'uses' => 'BlogController@show',
    'as' => 'show'
]);
Route::post('comments/{slug}',[
    'uses' => 'BlogController@comments',
    'as' => 'comments'
]);
Route::post('reply',[
    'uses' => 'BlogController@reply',
    'as' => 'reply'
]);
Route::get('catposts/{name}',[
    'uses' => 'BlogController@catposts',
    'as' =>'catposts'
]);


//login
Route::post('custom',[
    'uses' => 'LoginController@login',
    'as' => 'custom'
]);

//Oauth
Route::get('{provider}/auth',[
    'uses' => 'SocialsController@auth',
    'as' => 'socialaAuth'
]);
Route::get('/{provider}/redirect',[
    'uses' => 'SocialsController@socialcallback',
    'as' => 'socialcallback'
]);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
