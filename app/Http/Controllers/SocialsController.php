<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SocialAuth;

class SocialsController extends Controller
{
    public function auth($provider){

       return SocialAuth::authorize($provider);

    }

    public function socialcallback($provider){

        SocialAuth::login($provider,function($user,$data){
            //dd($data);
            $user->avatar = $data->avatar;
            $user->email = $data->email;
            $user->name = $data->full_name;
            $user->save();
        });

        return redirect()->route('blog');
    }
}
