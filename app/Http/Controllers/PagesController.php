<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Readmore;

class PagesController extends Controller
{
    public function index(){

        $post = Post::orderBy('created_at','desc')->limit(3)->get();

        return view('pages.index')->with('post',$post);
    }

    public function gallery(){
        return view('pages.gallery');
    }

    public function schedule(){
        return view('pages.schedule');
    }

    public function board(){
        return view('pages.theboard');
    }

    public function about(Request $request){

        $title = "about";

        $oputa = Readmore::find(1)->first();

        //dd($oputa);
        return view('pages.about')
                                ->with('oputa',$oputa)
                                ->with('title',$title);
    }

    public function speakers(){
        return view('pages.speakers2');
    }

    public function pricing(){
        return view('pages.pricing');
    }
    public function sponsors(){
        return view('pages.sponsors');
    }

    public function ajax(Request $request,$id){

        $data = Readmore::find($id);
        $data = json_encode($data);

        return $data;      
        
    }

    public function register(Request $request){

        dd($request->all());

    }

    public function programs(){
        return view('pages.programs');
    }

    public function sponsorship(){
        return view('pages.sponsorship');
    }

    public function patrons(){
        return view('pages.patrons');
    }

    public function committee(){
        return view('pages.committee');
    }
}
