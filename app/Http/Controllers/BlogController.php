<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Comment;
use App\Post;
use App\Reply;
use App\Category;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function index(){

        $pop = Post::all();

        $cat = Category::all();

        $post = Post::orderBy('created_at','desc')->simplePaginate(2);
        //dd();

        return view('pages.blog.index')->with('pop',$pop)
                                        ->with('cat',$cat)
                                        ->with('post',$post);
    }

    public function show(Request $request,$slug){

        //$pop = Post::all();

        $post = Post::where('slug',$slug)->first();
        
        $post->visit();

        

            
           // dd(Post::popularAllTime()->get()->take(3));

        //$comments = Comment::where('post_id',$post->id);

        //dd($comments->user_id);

        //$post->addView();

        return view('pages.blog.single')
                                        //->with('comments',$comments)
                                        //->with('pop',$pop)
                                        ->with('post',$post);
       
        //dd($post->getViews());
        //dd($sortedPosts);

    }

    public function comments(Request $request,$slug){

       if(Auth::guest()){

        //dd($request->all());
           return redirect()->route('login');


       }else{

           $user = Auth::user()->id;

           //dd($request->all());
        $this->validate($request, [
            'comment' => 'required|min:20',
            'post_id' => 'required|integer'
        ]);

        //dd($request->all());

        Comment::create([
            'post_id' => $request->post_id,
            'user_id' => Auth::user()->id,
            'comment' => $request->comment
        ]);

        Session::flash('success','your comment has been created successfully');

        return redirect()->back();

       }
        
    }

    public function reply(Request $request){

       // dd($request->all());

            if($request->commentId){

                $this->validate($request,[
                    'reply' => 'required'
                ]);

                Reply::create([
                    'comment_id' =>$request->commentId,
                    'user_id' => Auth::user()->id,
                    'reply' => $request->reply
                ]);

            }

            Session::flash('success','your reply has been created successfully'); 

            return redirect()->back();

        
    }

    public function catposts($name){

        $cat = Category::where('name',$name)->first();
        $catid = $cat->id;

        $catposts = Post::where('category_id',$catid)->get();

        return view('pages.blog.allpost')
                                        ->with('name',$name)
                                        ->with('catposts',$catposts);

    }
}
