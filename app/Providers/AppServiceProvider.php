<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Category;
use View;
use App\Post;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        //$popularPosts = Post::all()->orderByUniqueViewsCount('asc')->get();
        //$sortedPosts = Post::orderByUniqueViewsCount('asc')->first();

       $popular = Post::popularAllTime()->get()->take(3);
        $cat = Category::all();

        View::share(['cat' => $cat,'popular'=>$popular]);

        Schema::defaultStringLength(191);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
