<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \JordanMiguel\LaravelPopular\Traits\Visitable;


class Post extends Model
{
    use Visitable;

    public function scopePopularAllTime($query)
    {
        return $query->withCount('visits')->orderBy('visits_count', 'desc');
    }

    
    protected $fillable=[
        'title','content','category_id','image'
    ];


    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
